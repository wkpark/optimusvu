/* drivers/video/backlight/lm3533_bl.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/kernel.h>
#include <linux/spinlock.h>
#include <linux/backlight.h>
#include <linux/fb.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <mach/board.h>

#include <mach/board_lge.h>

#define MAX_BRIGHTNESS_lm3533 			0xFF
#define MAX_BRIGHTNESS_lm3528 			0x7F
#define DEFAULT_BRIGHTNESS 		0x33//220nit, shoogi.lee@lge.com, 2011_04_20
#define MIN_BRIGHTNESS 			0x04
#define I2C_BL_NAME "lm3533"

#define BL_ON	1
#define BL_OFF	0

static struct i2c_client *lm3533_i2c_client;

struct backlight_platform_data {
   void (*platform_init)(void);
   int gpio;
   unsigned int mode;
   int max_current;
   int init_on_boot;
   int min_brightness;
//start, linear mode, shoogi.lee@lge.com, 2011_04_20
   int default_brightness;
   int max_brightness;
//end, linear mode, shoogi.lee@lge.com, 2011_04_20   
};

struct lm3533_device {
	struct i2c_client *client;
	struct backlight_device *bl_dev;
	int gpio;
	int max_current;
	int min_brightness;
//start, linear mode, shoogi.lee@lge.com, 2011_04_20
	int default_brightness;
	int max_brightness;
//end, linear mode, shoogi.lee@lge.com, 2011_04_20 
	struct mutex bl_mutex;
};

static const struct i2c_device_id lm3533_bl_id[] = {
	{ I2C_BL_NAME, 0 },
	{ },
};

static int lm3533_write_reg(struct i2c_client *client, unsigned char reg, unsigned char val);

static int cur_main_lcd_level = DEFAULT_BRIGHTNESS;
static int saved_main_lcd_level = DEFAULT_BRIGHTNESS;

static int backlight_status = BL_OFF;
static struct lm3533_device *main_lm3533_dev = NULL;

static void lm3533_hw_reset(void)
{
	int gpio = main_lm3533_dev->gpio;	
	
	gpio_tlmm_config(GPIO_CFG(gpio, 0, GPIO_CFG_OUTPUT, GPIO_CFG_PULL_UP, GPIO_CFG_2MA),GPIO_CFG_ENABLE);
	gpio_set_value(gpio, 1);
	mdelay(1);
}

static int lm3533_write_reg(struct i2c_client *client, unsigned char reg, unsigned char val)
{
	int err;
	u8 buf[2];
	struct i2c_msg msg = {
		client->addr, 0, 2, buf
	};

	buf[0] = reg;
	buf[1] = val;
	
	if ((err = i2c_transfer(client->adapter, &msg, 1)) < 0) {
		dev_err(&client->dev, "i2c write error\n");
	}
	
	return 0;
}

extern int lge_bd_rev;

static void lm3533_set_main_current_level(struct i2c_client *client, int level)
{
	struct lm3533_device *dev;
	int cal_value=0;
	
	int min_brightness = main_lm3533_dev->min_brightness;
	//int max_current = main_lm3533_dev->max_current;
	//int default_brightness=main_lm3533_dev->default_brightness;
	int max_brightness=main_lm3533_dev->max_brightness;

   dev = (struct lm3533_device *)i2c_get_clientdata(client);
	cur_main_lcd_level = level; 
	dev->bl_dev->props.brightness = cur_main_lcd_level;

	mutex_lock(&main_lm3533_dev->bl_mutex);

	if(level!= 0){
		if(lge_bd_rev >= LGE_REV_C) {
#if 0 //EJJ_S For IBC ej.jung@lge.com 2011-11-28 IBC(Multi-ALC)
			if (level < MIN_BRIGHTNESS)
				cal_value = min_brightness;
			else if (MIN_BRIGHTNESS <= level && level < 30)
				cal_value = 23;
			else if (30 <= level && level < 57) //55(1)
				cal_value = 27;
			else if (57<= level && level < 63) //60(2)
				cal_value = 31;
			else if (63<= level && level < 69) //65 (3)
				cal_value = 41;
			else if (69<= level && level < 76) //(4) 73
				cal_value = 48;
			else if (76<= level && level < 88) //(5) 80
				cal_value = 51;
			else if (88<= level && level < 98) //(6) 96
				cal_value = 63;
			else if (98<= level && level < 105) //(7) 100
				cal_value = 68;
			else if (105<= level && level < 113) //(8) 110
				cal_value = 84;
			else if (113<= level && level < 118) //(9) 116
				cal_value = 84;
			else if (118<= level && level < 122) //(10) 120
				cal_value = 84;
			else if (122<= level && level < 124) //(11) 123
				cal_value = 98;
			else if (124<= level && level < 126) //(12) 125
				cal_value = 99;
			else if (126<= level && level < 129) //(13) 127
				cal_value = 102;
			else if (129<= level && level < 135) //(14) 130
				cal_value = 125;
			else if (135<= level && level < 154) //(15) 140
				cal_value = 140;
			else if (154<= level && level < 179) //(16) 168
				cal_value = 155;
			else if (179<= level && level < 200) //(17) 190
				cal_value = 163;
			else if (200<= level && level < 218) //(18) 210
				cal_value = 170;
			else if (218<= level && level < 230) //(19) 226
				cal_value = 188;
			else if (230<= level && level < 235)
				cal_value = 200;
			else if (235<= level && level < 255)
				cal_value = level;
			else if (level >= MAX_BRIGHTNESS_lm3533)
				cal_value = max_brightness;
#endif //EJJ_E ej.jung@lge.com 2011-11-28 IBC(Multi-ALC)
//EJJ_S IBC ej.jung@lge.com 2011-11-28 IBC(Multi-ALC)
			if(level >= 1 && level <= 255)
			{
				cal_value = (((level*2250-30*2250)/225)*251 + 4*2250)/2250; //input min=30, Max255,,, Output min 20 ; max 255
				//if(cal_value < 165)
				//	cal_value = (cal_value*108)/100 - 38;
				//else
				//	cal_value = (cal_value*150)/100 - 100;
			}
			else{
				mdelay(1);
				return;
			}

			if (cal_value < MIN_BRIGHTNESS){
				cal_value = min_brightness;
				cal_value = MIN_BRIGHTNESS;
			}
			else if (cal_value > MAX_BRIGHTNESS_lm3533)
			{
				cal_value = max_brightness;
				cal_value = MAX_BRIGHTNESS_lm3533;
			}

			pr_debug("................... level=%d,cal_value1=%d\n",level,cal_value);
//EJJ_E ej.jung@lge.com 2011-11-28 IBC(Multi-ALC)
			lm3533_write_reg(main_lm3533_dev->client, 0x40, cal_value);
		}
		else{
			if(level < MIN_BRIGHTNESS)
				cal_value = 50;
			else if (MIN_BRIGHTNESS <= level && level < 15)
				cal_value = 80;
			else if (15<= level && level < 20) //level = 18 (power saving)
				cal_value = 82;
			else if (20<= level && level < 23)
				cal_value = 84;
			else if (23<= level && level < 28) //level = 26
				cal_value = 86;
			else if (28<= level && level < 31) //level = 29 (power saving)
				cal_value = 90;
			else if (31<= level && level < 35) //level =32
				cal_value = 97;
			else if (35<= level && level < 42) //level =40 (power saving)
				cal_value = 101;
			else if (42<= level && level < 48) //level = 45 (power saving)
				cal_value = 105;
			else if (48<= level && level < 53) //level = 51
				cal_value = 106;
			else if (53<= level && level < 55) //level = 54 (power saving)
				cal_value = 107;
			else if (55<= level && level < 57) //level = 56 (power saving)
				cal_value = 109;
			else if (57<= level && level < 60) //level = 58 (power saving)
				cal_value = 110;
			else if (60<= level && level < 73) //level = 61
				cal_value = 114;
			else if (73<= level && level < 95) //level = 84
				cal_value = 118;
			else if (95<= level && level < 109) //level = 105
				cal_value = 120;
			else if (109<= level && level < 120) //level =113
				cal_value = 122;
			else if (120<= level && level < MAX_BRIGHTNESS_lm3528)
				cal_value = level;
			else if (level >= MAX_BRIGHTNESS_lm3528)
				cal_value = 127;
			//printk ("ch.han=========level=%d,cal_value1=%d\n",level,cal_value);
			lm3533_write_reg(client, 0xA0, cal_value);
		}
	}
	else{
		if(lge_bd_rev >= LGE_REV_C){
			pr_debug("................... level=%d,cal_value1=%d\n",level,cal_value);
			lm3533_write_reg(client, 0x27, 0x00); //Control Bank is disabled
		}
		else
			lm3533_write_reg(client, 0x10, 0x00);
	}

	//msleep(1);

	mutex_unlock(&main_lm3533_dev->bl_mutex);
}

void lm3533_backlight_on(int level)
{
	//printk("%s received (prev backlight_status: %s)\n", __func__, backlight_status?"ON":"OFF");
	if(backlight_status == BL_OFF){
		if(lge_bd_rev >= LGE_REV_C){
			lm3533_hw_reset();
			lm3533_write_reg(main_lm3533_dev->client, 0x10, 0x0); //HVLED 1 & 2 are controlled by Bank A
			//lm3533_write_reg(main_lm3533_dev->client, 0x14, 0x1); //PWM input is enabled
			lm3533_write_reg(main_lm3533_dev->client, 0x1A, 0x2); //Linear & Control Bank A is configured for register Current control
			lm3533_write_reg(main_lm3533_dev->client, 0x1F, 0x13); //Full-Scale Current (20.2mA)
			lm3533_write_reg(main_lm3533_dev->client, 0x27, 0x1); //Control Bank A is enable
			lm3533_write_reg(main_lm3533_dev->client, 0x2C, 0xA); //Active High, OVP(40V), Boost Frequency(1Mhz)
		}
		else{
			lm3533_hw_reset();
			lm3533_write_reg(main_lm3533_dev->client, 0x10, 0x5);
		}
	}

	lm3533_set_main_current_level(main_lm3533_dev->client, level);
	backlight_status = BL_ON;

	return;
}

void lm3533_backlight_off(void)
{
	int gpio = main_lm3533_dev->gpio;

	if (backlight_status == BL_OFF) return;
	saved_main_lcd_level = cur_main_lcd_level;
	lm3533_set_main_current_level(main_lm3533_dev->client, 0);
	backlight_status = BL_OFF;

	gpio_tlmm_config(GPIO_CFG(gpio, 0, GPIO_CFG_OUTPUT, GPIO_CFG_PULL_UP, GPIO_CFG_2MA),GPIO_CFG_ENABLE);
	gpio_direction_output(gpio, 0);
	msleep(6);
	
	return;
}

void lm3533_lcd_backlight_set_level( int level)
{
	if(lge_bd_rev >= LGE_REV_C){
		if (level > MAX_BRIGHTNESS_lm3533)
			level = MAX_BRIGHTNESS_lm3533;
	}
	else{
		if (level > MAX_BRIGHTNESS_lm3528)
			level = MAX_BRIGHTNESS_lm3528;
	}

	if(lm3533_i2c_client!=NULL )
	{		
		if(level == 0) {
			lm3533_backlight_off();
		} else {
			lm3533_backlight_on(level);
		}

		//printk("%s() : level is : %d\n", __func__, level);
	}else{
		printk("%s(): No client\n",__func__);
	}
}
EXPORT_SYMBOL(lm3533_lcd_backlight_set_level);

static int bl_set_intensity(struct backlight_device *bd)
{
	
	struct i2c_client *client = to_i2c_client(bd->dev.parent);

	lm3533_set_main_current_level(client, bd->props.brightness);
	cur_main_lcd_level = bd->props.brightness; 
	
	return 0;
}

static int bl_get_intensity(struct backlight_device *bd)
{
    unsigned char val=0;
	 val &= 0x1f;
    return (int)val;
}

static ssize_t lcd_backlight_show_level(struct device *dev, struct device_attribute *attr, char *buf)
{
	int r;

	r = snprintf(buf, PAGE_SIZE, "LCD Backlight Level is : %d\n", cur_main_lcd_level);
	
	return r;
}

static ssize_t lcd_backlight_store_level(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int level;
	struct i2c_client *client = to_i2c_client(dev); 

	if (!count)
		return -EINVAL;
	
	level = simple_strtoul(buf, NULL, 10);

	if(lge_bd_rev >= LGE_REV_C){
		if (level > MAX_BRIGHTNESS_lm3533)
			level = MAX_BRIGHTNESS_lm3533;
	}
	else {
		if (level > MAX_BRIGHTNESS_lm3528)
			level = MAX_BRIGHTNESS_lm3528;
	}

	lm3533_set_main_current_level(client, level);
	cur_main_lcd_level = level; 
	
	return count;
}

static int lm3533_bl_resume(struct i2c_client *client)
{
    lm3533_backlight_on(saved_main_lcd_level);
    
    return 0;
}

static int lm3533_bl_suspend(struct i2c_client *client, pm_message_t state)
{
    printk(KERN_INFO"%s: new state: %d\n",__func__, state.event);

    lm3533_backlight_off();

    return 0;
}

static ssize_t lcd_backlight_show_on_off(struct device *dev, struct device_attribute *attr, char *buf)
{
	int r = 0;

	printk("%s received (prev backlight_status: %s)\n", __func__, backlight_status?"ON":"OFF");

	return r;
}

static ssize_t lcd_backlight_store_on_off(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int on_off;
	struct i2c_client *client = to_i2c_client(dev); 

	if (!count)
		return -EINVAL;
	
	printk("%s received (prev backlight_status: %s)\n", __func__, backlight_status?"ON":"OFF");
	
	on_off = simple_strtoul(buf, NULL, 10);
	
	printk(KERN_ERR "%d",on_off);
	
	if(on_off == 1){
		lm3533_bl_resume(client);
	}else if(on_off == 0)
	    lm3533_bl_suspend(client, PMSG_SUSPEND);
	
	return count;

}
DEVICE_ATTR(lm3533_level, 0644, lcd_backlight_show_level, lcd_backlight_store_level);
DEVICE_ATTR(lm3533_backlight_on_off, 0644, lcd_backlight_show_on_off, lcd_backlight_store_on_off);

static struct backlight_ops lm3533_bl_ops = {
	.update_status = bl_set_intensity,
	.get_brightness = bl_get_intensity,
};

static int lm3533_probe(struct i2c_client *i2c_dev, const struct i2c_device_id *id)
{
	struct backlight_platform_data *pdata;
	struct lm3533_device *dev;
	struct backlight_device *bl_dev;
	struct backlight_properties props;
	int err;

	printk(KERN_INFO"%s: i2c probe start\n", __func__);

	pdata = i2c_dev->dev.platform_data;
	lm3533_i2c_client = i2c_dev;

	dev = kzalloc(sizeof(struct lm3533_device), GFP_KERNEL);
	if(dev == NULL) {
		dev_err(&i2c_dev->dev,"fail alloc for lm3533_device\n");
		return 0;
	}

	main_lm3533_dev = dev;

	memset(&props, 0, sizeof(struct backlight_properties));
	if(lge_bd_rev >= LGE_REV_C){
		props.max_brightness = MAX_BRIGHTNESS_lm3533;	
		bl_dev = backlight_device_register(I2C_BL_NAME, &i2c_dev->dev, NULL, &lm3533_bl_ops, &props);
		bl_dev->props.max_brightness = MAX_BRIGHTNESS_lm3533;
		bl_dev->props.brightness = DEFAULT_BRIGHTNESS;
		bl_dev->props.power = FB_BLANK_UNBLANK;
	}
	else {
		props.max_brightness = MAX_BRIGHTNESS_lm3528;	
		bl_dev = backlight_device_register(I2C_BL_NAME, &i2c_dev->dev, NULL, &lm3533_bl_ops, &props);
		bl_dev->props.max_brightness = MAX_BRIGHTNESS_lm3528;
		bl_dev->props.brightness = DEFAULT_BRIGHTNESS;
		bl_dev->props.power = FB_BLANK_UNBLANK;
	}
	dev->bl_dev = bl_dev;
	dev->client = i2c_dev;
	dev->gpio = pdata->gpio;
	dev->max_current = pdata->max_current;
	dev->min_brightness = pdata->min_brightness;
	//start, linear mode, shoogi.lee@lge.com, 2011_04_20
	dev->default_brightness = pdata->default_brightness;
	dev->max_brightness = pdata->max_brightness;
	//end, linear mode, shoogi.lee@lge.com, 2011_04_20
	i2c_set_clientdata(i2c_dev, dev);

	if(dev->gpio && gpio_request(dev->gpio, "lm3533 reset") != 0) {
		return -ENODEV;
	}

	mutex_init(&dev->bl_mutex);

#ifdef CONFIG_LGE_BOOTLOADER_DISP_INIT
#else
	lm3533_hw_reset();
	lm3533_backlight_on(DEFAULT_BRIGHTNESS);
#endif

	err = device_create_file(&i2c_dev->dev, &dev_attr_lm3533_level);
	err = device_create_file(&i2c_dev->dev, &dev_attr_lm3533_backlight_on_off);

	return 0;
}

static int lm3533_remove(struct i2c_client *i2c_dev)
{
	struct lm3533_device *dev;
	int gpio = main_lm3533_dev->gpio;

 	device_remove_file(&i2c_dev->dev, &dev_attr_lm3533_level);
 	device_remove_file(&i2c_dev->dev, &dev_attr_lm3533_backlight_on_off);
	dev = (struct lm3533_device *)i2c_get_clientdata(i2c_dev);
	backlight_device_unregister(dev->bl_dev);
	i2c_set_clientdata(i2c_dev, NULL);

	if (gpio_is_valid(gpio))
		gpio_free(gpio);
	return 0;
}	

static struct i2c_driver main_lm3533_driver = {
	.probe = lm3533_probe,
	.remove = lm3533_remove,
	.suspend = NULL,
	.resume = NULL,
	.id_table = lm3533_bl_id, 
	.driver = {
		.name = I2C_BL_NAME,
		.owner = THIS_MODULE,
	},
};


static int __init lcd_backlight_init(void)
{
	static int err=0;

	err = i2c_add_driver(&main_lm3533_driver);

	return err;
}
 
module_init(lcd_backlight_init);

MODULE_DESCRIPTION("LM3533 Backlight Control");
MODULE_AUTHOR("Jaeseong Gim <jaeseong.gim@lge.com>");
MODULE_LICENSE("GPL");
