/* drivers/input/keyboard/synaptics_i2c_rmi.c
 *
 * Copyright (C) 2007 Google, Inc.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/module.h>
#include <linux/delay.h>
#include <linux/earlysuspend.h>
#include <linux/hrtimer.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <mach/gpio.h>
#include <linux/jiffies.h>
#include <linux/slab.h>

#include <linux/workqueue.h>	//20101221 seven.kim@lge.com to use real time work queue
#include <linux/delay.h> //20101221 seven.kim@lge.com to use mdelay
#include <linux/wakelock.h> 	//20102121 seven.kim@lge.com to use wake_lock

#include "f100_synaptics_ts.h"

//#include "synaptics_ts_firmware.h"
//#include "synaptics_ts_firmware_lgit.h"
//#define SYNAPTICS_SUPPORT_FW_UPGRADE

/*<sunggyun.yu@lge.com> enable this for printk message*/
//#define DEBUG

#if 0 //seven for debugging
#define pr_debug(fmt, ...) \
	printk(KERN_DEBUG pr_fmt(fmt), ##__VA_ARGS__)
#endif

#if 1 //seven open for test
#define FEATURE_LGE_TOUCH_MOVING_IMPROVE
#define FEATURE_LGE_TOUCH_JITTERING_IMPROVE
#endif //end of seven

#define FEATURE_LGE_TOUCH_GRIP_SUPPRESSION //20101215 seven.kim@lge.com

#define FEATURE_LGE_TOUCH_REAL_TIME_WORK_QUEUE	//20101221 seven.kim@lge.com to use real time work queue
#define FEATURE_LGE_TOUCH_ESD_DETECT					//20101221 seven.kim@lge.com to detect Register change by ESD
/*===========================================================================
                DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains definitions for constants, macros, types, variables
and other items needed by this module.
===========================================================================*/

static struct workqueue_struct *synaptics_wq;
// 20100826 jh.koo@lge.com, for stable initialization [START_LGE]
static struct i2c_client *hub_ts_client = NULL;
// 20100826 jh.koo@lge.com, for stable initialization [END_LGE]

static int init_stabled = -1;

#ifdef CONFIG_HAS_EARLYSUSPEND
static void synaptics_ts_early_suspend(struct early_suspend *h);
static void synaptics_ts_late_resume(struct early_suspend *h);
#endif

static void synaptics_ts_suspend_func(struct synaptics_ts_data *ts);
static void synaptics_ts_resume_func(struct synaptics_ts_data *ts);

#define TOUCH_INT_N_GPIO						61

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*                                                                         */
/*                                 Macros                                  */
/*                                                                         */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

#define TS_SNTS_GET_FINGER_STATE_0(finger_status_reg) \
		(finger_status_reg&0x03)
#define TS_SNTS_GET_FINGER_STATE_1(finger_status_reg) \
		((finger_status_reg&0x0C)>>2)
#define TS_SNTS_GET_FINGER_STATE_2(finger_status_reg) \
		((finger_status_reg&0x30)>>4)
#define TS_SNTS_GET_FINGER_STATE_3(finger_status_reg) \
      ((finger_status_reg&0xC0)>>6)
#define TS_SNTS_GET_FINGER_STATE_4(finger_status_reg) \
      (finger_status_reg&0x03)

#define TS_SNTS_GET_X_POSITION(high_reg, low_reg) \
		((int)(high_reg*0x10) + (int)(low_reg&0x0F))
#define TS_SNTS_GET_Y_POSITION(high_reg, low_reg) \
		((int)(high_reg*0x10) + (int)((low_reg&0xF0)/0x10))

#define TS_SNTS_GET_REPORT_RATE(device_control_reg) \
		((device_control_reg&0x40)>>6)
// 1st bit : '0' - Allow sleep mode, '1' - Full power without sleeping
// 2nd and 3rd bit : 0x00 - Normal Operation, 0x01 - Sensor Sleep
#define TS_SNTS_GET_SLEEP_MODE(device_control_reg) \
		(device_control_reg&0x07)

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*                                                                         */
/*                       CONSTANTS DATA DEFINITIONS                        */
/*                                                                         */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

#define TOUCH_EVENT_NULL						0
#define TOUCH_EVENT_BUTTON						1
#define TOUCH_EVENT_ABS							2

#define SYNAPTICS_TM1940_PRODUCT_ID				"TM1940"
#define SYNAPTICS_TM1940_RESOLUTION_X			1572
#define SYNAPTICS_TM1940_RESOLUTION_Y			2244
#define SYNAPTICS_TM1940_LCD_ACTIVE_AREA			2096
#define SYNAPTICS_TM1940_BUTTON_ACTIVE_AREA		2097


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*                                                                         */
/*                    REGISTER ADDR & SETTING VALUE                        */
/*                                                                         */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

#define SYNAPTICS_FLASH_CONTROL_REG				0x12
#define SYNAPTICS_DATA_BASE_REG					0x13
#define SYNAPTICS_INT_STATUS_REG				0x14

#define SYNAPTICS_CONTROL_REG						0x4F	//20101221 seven changed 0x4F -> 0x4C
#define SYNAPTICS_RIM_CONTROL_INTERRUPT_ENABLE	0x50	//20101227 seven added to prevent interrupt in booting time

#define SYNAPTICS_DELTA_X_THRES_REG				0x58 	//20101221 seven changed 0x53 -> 0x50
#define SYNAPTICS_DELTA_Y_THRES_REG				0x59 	//20101221 seven changed 0x53 -> 0x51

#define SYNAPTICS_FW_REVISION_REG				0x8B

#define SYNAPTICS_RMI_QUERY_BASE_REG			0xE3
#define SYNAPTICS_RMI_CMD_BASE_REG				0xE4
#define SYNAPTICS_FLASH_QUERY_BASE_REG			0xE9
#define SYNAPTICS_FLASH_DATA_BASE_REG			0xEC

#define SYNAPTICS_INT_FLASH						1<<0
#define SYNAPTICS_INT_STATUS					1<<1
#define SYNAPTICS_INT_ABS0						1<<2

#define SYNAPTICS_CONTROL_SLEEP					1<<0
#define SYNAPTICS_CONTROL_NOSLEEP				1<<2

#ifdef FEATURE_LGE_TOUCH_ESD_DETECT //20101221 seven.kim@lge.com to detect Register change by ESD
#define SYNAPTICS_CONTROL_CONFIGURED				1<<7
#define SYNAPTICS_RIM_DEVICE_RESET				1<<0
#endif /*FEATURE_LGE_TOUCH_ESD_DETECT*/

#ifdef SYNAPTICS_SUPPORT_FW_UPGRADE
#define SYNAPTICS_FLASH_CMD_FW_CRC				0x01
#define SYNAPTICS_FLASH_CMD_FW_WRITE			0x02
#define SYNAPTICS_FLASH_CMD_ERASEALL			0x03
#define SYNAPTICS_FLASH_CMD_CONFIG_READ			0x05
#define SYNAPTICS_FLASH_CMD_CONFIG_WRITE		0x06
#define SYNAPTICS_FLASH_CMD_CONFIG_ERASE		0x07
#define SYNAPTICS_FLASH_CMD_ENABLE				0x0F
#define SYNAPTICS_FLASH_NORMAL_RESULT			0x80
#define FW_IMAGE_SIZE 	28929
unsigned char SynapticsFirmware[FW_IMAGE_SIZE];
#endif /* SYNAPTICS_SUPPORT_FW_UPGRADE */


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*                                                                         */
/*                         DATA DEFINITIONS                                */
/*                                                                         */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

static ts_sensor_data ts_reg_data;
static ts_finger_data prev_ts_data;
static ts_finger_data curr_ts_data;


static uint8_t curr_event_type = TOUCH_EVENT_NULL;
static uint8_t prev_event_type = TOUCH_EVENT_NULL;

static uint16_t pressed_button_type = KEY_REJECT;

static uint16_t SYNAPTICS_PANEL_MAX_X;
static uint16_t SYNAPTICS_PANEL_MAX_Y;
static uint16_t SYNAPTICS_PANEL_LCD_MAX_Y;
static uint16_t SYNAPTICS_PANEL_BUTTON_MIN_Y;

unsigned char  touch_fw_version = 0;

//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time.
int sent_event[MAX_NUM_OF_FINGER] = {0,};

struct wake_lock ts_wake_lock; //20101221 seven.kim@lge.com for ts recovery

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*                                                                         */
/*                           Local Functions                               */
/*                                                                         */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

// 20101022 joseph.jung@lge.com touch smooth moving improve [START]
#ifdef FEATURE_LGE_TOUCH_MOVING_IMPROVE
#define ADJUST_VAL				4
#define ADJUST_BASIS_VALUE		20
#define Square(x) ((x) * (x))

static void touch_adjust_position(int finger_num)
{
	if(prev_ts_data.touch_status[finger_num])
	{
		if( (Square(prev_ts_data.X_position[finger_num] - curr_ts_data.X_position[finger_num]) + Square(prev_ts_data.Y_position[finger_num] - curr_ts_data.Y_position[finger_num])) <= Square(ADJUST_BASIS_VALUE) )
		{
			curr_ts_data.X_position[finger_num] = (prev_ts_data.X_position[finger_num] * ADJUST_VAL + curr_ts_data.X_position[finger_num]) / (ADJUST_VAL + 1);
			curr_ts_data.Y_position[finger_num] = (prev_ts_data.Y_position[finger_num] * ADJUST_VAL + curr_ts_data.Y_position[finger_num]) / (ADJUST_VAL + 1);
		}
	}
}
#endif /* FEATURE_LGE_TOUCH_MOVING_IMPROVE */
// 20101022 joseph.jung@lge.com touch smooth moving improve [END]

#ifdef FEATURE_LGE_TOUCH_ESD_DETECT //20101221 seven.kim@lge.com to detect Register change by ESD
static void Synatics_ts_touch_Recovery(struct synaptics_ts_data *ts);
#endif /*FEATURE_LGE_TOUCH_ESD_DETECT*/


/* [START] seven.kim@lge.com To avoid touch sensor lock-up & data missing in touch release*/
static uint32_t  Synaptics_Check_Touch_Interrupt_Status(void)
{
		uint32_t pinValue = 0;
		pinValue = gpio_get_value(TOUCH_INT_N_GPIO);

		return !pinValue;
}
/* [END] seven.kim@lge.com To avoid touch sensor lock-up & data missing in touch release*/
/* [START] seven.kim@lge.com to recover in case of I2C Fail */
int g_touch_read_cnt = 0; 		//20110407 ATS TEST FAIL
int g_touch_restart_flag = 0;		//20110407 ATS TEST FAIL
s32 synaptics_ts_i2c_read_block_data(struct i2c_client *client, u8 command,
				  u8 length, u8 *values)
{
	s32 status;
	status = i2c_smbus_read_i2c_block_data(client, command, length, values);

	if (status < 0)
	{
		struct synaptics_ts_data *ts = i2c_get_clientdata(client);

#if 1 //20110309 seven for late_resume_lcd
		g_touch_read_cnt++;
		if(g_touch_restart_flag ==0 && g_touch_read_cnt > 3)
		{
			printk("%s : Int Pin : %s \n",__func__, Synaptics_Check_Touch_Interrupt_Status()?"LOW":"HIGH");

			g_touch_restart_flag = 1;
		        Synatics_ts_touch_Recovery(ts);
			g_touch_restart_flag = 0;
			g_touch_read_cnt = 0;

			schedule_delayed_work(&ts->init_delayed_work, msecs_to_jiffies(400));
		}
#endif //20110309 seven for late_resume_lcd

	}
	else
		g_touch_read_cnt = 0;

	return status;
}

s32 synaptics_ts_i2c_read_byte_data(struct i2c_client *client, u8 command)
{
	s32 status;
	status = i2c_smbus_read_byte_data(client, command);

	return status;
}

s32 synaptics_ts_i2c_write_block_data(struct i2c_client *client, u8 command,
			       u8 length, const u8 *values)
{
	s32 status;

	status = i2c_smbus_write_block_data(client , command, length, values );

	return status;
}
s32 synaptics_ts_i2c_write_byte_data(struct i2c_client *client, u8 command, u8 value)
{
	s32 status;

	status = i2c_smbus_write_byte_data(client, command, value); /* wake up */

	return status;
}
/* [END] seven.kim@lge.com to recover in case of I2C Fail */

// 20100826 jh.koo@lge.com, for stable initialization [START_LGE]
static void synaptics_ts_init_delayed_work(struct work_struct *work)
{
	int ret;

	//pr_warning("%s() : Touch Delayed Work : Start!!\n", __func__);

	{
		uint32_t  pinValue = 0;
		pinValue = Synaptics_Check_Touch_Interrupt_Status();
		if(pinValue)
		{
			//uint8_t dummy_read = 0;
			ts_sensor_data tmp_ts_reg_data;

			synaptics_ts_i2c_read_block_data(hub_ts_client, SYNAPTICS_DATA_BASE_REG, sizeof(tmp_ts_reg_data), (u8 *)&tmp_ts_reg_data);
		}
	}
	/* [END] seven.kim@lge.com to avoid touch lockup in case of HW_Rev_B and C */

	//[START] 20101226 seven.kim@lge.com to prevent touch interrupt in booting time
	synaptics_ts_i2c_write_byte_data(hub_ts_client, SYNAPTICS_RIM_CONTROL_INTERRUPT_ENABLE, 0x00); //interrupt disable

	disable_irq(hub_ts_client->irq);
	//[END] 20101226 seven.kim@lge.com to prevent touch interrupt in booting time

	synaptics_ts_i2c_read_block_data(hub_ts_client, SYNAPTICS_DATA_BASE_REG, sizeof(ts_reg_data), (u8 *)&ts_reg_data);

	/* [START] 20110110 seven.kim@lge.com touch sensing test F/Up */
	//if(system_rev < 4)
	//{
	      synaptics_ts_i2c_write_byte_data(hub_ts_client, SYNAPTICS_CONTROL_REG, SYNAPTICS_CONTROL_NOSLEEP); /* wake up */
		synaptics_ts_i2c_write_byte_data(hub_ts_client, SYNAPTICS_CONTROL_REG, SYNAPTICS_CONTROL_CONFIGURED );
	//}
	//else
	//{
	synaptics_ts_i2c_write_byte_data(hub_ts_client, SYNAPTICS_CONTROL_REG, (SYNAPTICS_CONTROL_CONFIGURED | SYNAPTICS_CONTROL_NOSLEEP));
	//}
	/* [START] 20110110 seven.kim@lge.com touch sensing test F/Up */

// 20101022 joseph.jung@lge.com touch smooth moving improve [START]
#ifdef FEATURE_LGE_TOUCH_MOVING_IMPROVE
	ret = synaptics_ts_i2c_write_byte_data(hub_ts_client, SYNAPTICS_DELTA_X_THRES_REG, 0x01);
	ret = synaptics_ts_i2c_write_byte_data(hub_ts_client, SYNAPTICS_DELTA_Y_THRES_REG, 0x01);
#endif /* FEATURE_LGE_TOUCH_MOVING_IMPROVE */
// 20101022 joseph.jung@lge.com touch smooth moving improve [END]

	init_stabled = 1;

	//[START] 20101226 seven.kim@lge.com to prevent touch interrupt in booting time
	enable_irq(hub_ts_client->irq);
	synaptics_ts_i2c_write_byte_data(hub_ts_client, SYNAPTICS_RIM_CONTROL_INTERRUPT_ENABLE, 0x07); //interrupt enable
	//[END] 20101226 seven.kim@lge.com to prevent touch interrupt in booting time

	//pr_warning("%s() : Touch Delayed Work : End!!\n", __func__);
}
// 20100826 jh.koo@lge.com, for stable initialization [END_LGE]

// 20101215 seven@lge.com grip suppression [START]
#ifdef FEATURE_LGE_TOUCH_GRIP_SUPPRESSION
static int g_gripIgnoreRangeValue = 0;
static int g_receivedPixelValue = 0;

static int touch_ConvertPixelToRawData(int pixel)
{
	int result = 0;

	result = (SYNAPTICS_PANEL_MAX_X * pixel) /480;

	return result;
}

ssize_t touch_gripsuppression_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	sprintf(buf, "%d\n", g_receivedPixelValue);
	pr_debug("[KERNEL] [TOUCH] SHOW (%d) \n", g_receivedPixelValue);

	return (ssize_t)(strlen(buf)+1);

}

ssize_t touch_gripsuppression_store(struct device *dev, struct device_attribute *attr, const char *buffer, size_t count)
{
	sscanf(buffer, "%d", &g_receivedPixelValue);
	g_gripIgnoreRangeValue = touch_ConvertPixelToRawData(g_receivedPixelValue);
	pr_debug("[KERNEL] [TOUCH] STORE  pixel(%d) Convet (%d) \n", g_receivedPixelValue, g_gripIgnoreRangeValue);

	return count;
}

DEVICE_ATTR(gripsuppression, 0664, touch_gripsuppression_show, touch_gripsuppression_store);
#endif /* FEATURE_LGE_TOUCH_GRIP_SUPPRESSION */
// 20101215 seven@lge.com grip suppression [END]



static void synaptics_ts_work_func(struct work_struct *work)
{
	struct synaptics_ts_data *ts = container_of(to_delayed_work(work), struct synaptics_ts_data, work);
	int i;
	int finger_count = 0;

	//20110621 yongman.kwon@lge.com [LS855] adding to prevent ESD.
	unsigned char statusReg;

	//pr_info("[TOUCH] synaptics_ts_work_func() : START \n" );

	statusReg = synaptics_ts_i2c_read_byte_data(ts->client, SYNAPTICS_DATA_BASE_REG);

	//20110621 yongman.kwon@lge.com [LS855] adding to prevent ESD. [START]
	//0x03 means ESD detect from IC.

	//pr_info("[Touch] statusReg : %d\n", statusReg);

	if((statusReg&0x03) == 0x03)
	{
	pr_info("[Touch] IC Reset\n");

		Synatics_ts_touch_Recovery(ts);
		schedule_delayed_work(&ts->init_delayed_work, msecs_to_jiffies(400));
		memset(&ts_reg_data, 0x0, sizeof(ts_sensor_data));
		memset(&prev_ts_data, 0x0, sizeof(ts_finger_data));
		memset(&curr_ts_data, 0x0, sizeof(ts_finger_data));
	}
	else if((statusReg&0x80) == 0x80)
	{
	//pr_info("[Touch] un configured detect \n");

		schedule_delayed_work(&ts->init_delayed_work, msecs_to_jiffies(400));
		memset(&ts_reg_data, 0x0, sizeof(ts_sensor_data));
		memset(&prev_ts_data, 0x0, sizeof(ts_finger_data));
		memset(&curr_ts_data, 0x0, sizeof(ts_finger_data));
	}
	//20110621 yongman.kwon@lge.com [LS855] adding to prevent ESD. [END]


/* [START] seven.kim@lge.com To avoid touch sensor lock-up & data missing in touch release */
do{
/* [END] seven.kim@lge.com To avoid touch sensor lock-up & data missing in touch release */
	synaptics_ts_i2c_read_block_data(ts->client, SYNAPTICS_DATA_BASE_REG, sizeof(ts_reg_data), (u8 *)&ts_reg_data);

	//yongman.kwon
	finger_count = 0;

	if(ts_reg_data.interrupt_status_reg & SYNAPTICS_INT_ABS0)
	{
		for(i = 0; i < MAX_NUM_OF_FINGER; i++)
		{
			int check = 1 << ((i%4)*2);
			prev_ts_data.touch_status[i] = curr_ts_data.touch_status[i];
			prev_ts_data.X_position[i] = curr_ts_data.X_position[i];
			prev_ts_data.Y_position[i] = curr_ts_data.Y_position[i];
			prev_ts_data.width[i] = curr_ts_data.width[i];
			prev_ts_data.pressure[i] = curr_ts_data.pressure[i];

			if(((ts_reg_data.finger_state_reg[i/4] & check) == check)||(ts_reg_data.finger_state_reg[i]== 2))
			//if((ts_reg_data.finger_state_reg[i]== 1)||(ts_reg_data.finger_state_reg[i]== 2))
			{
				curr_ts_data.X_position[i] = (int)TS_SNTS_GET_X_POSITION(ts_reg_data.fingers_data[i][0], ts_reg_data.fingers_data[i][2]);
				curr_ts_data.Y_position[i] = (int)TS_SNTS_GET_Y_POSITION(ts_reg_data.fingers_data[i][1], ts_reg_data.fingers_data[i][2]);

// 20101022 joseph.jung@lge.com touch smooth moving improve [START]
#ifdef FEATURE_LGE_TOUCH_MOVING_IMPROVE
				touch_adjust_position(i);
#endif

#ifdef FEATURE_LGE_TOUCH_JITTERING_IMPROVE
				if(!(abs(curr_ts_data.X_position[i]-prev_ts_data.X_position[i]) > 1 && abs(curr_ts_data.Y_position[i]-prev_ts_data.Y_position[i]) > 1))
				{
					curr_ts_data.X_position[i] = prev_ts_data.X_position[i];
					curr_ts_data.Y_position[i] = prev_ts_data.Y_position[i];
				}
#endif
// 20101022 joseph.jung@lge.com touch smooth moving improve [END]

				#ifdef FEATURE_LGE_TOUCH_GRIP_SUPPRESSION // 20101215 seven@lge.com grip suppression [start]
				if ( (g_gripIgnoreRangeValue > 0) && ( (curr_ts_data.X_position[i] <= g_gripIgnoreRangeValue ) ||
															(curr_ts_data.X_position[i] >= (SYNAPTICS_PANEL_MAX_X - g_gripIgnoreRangeValue) )) )
				{
					pr_debug("[TOUCH] Girp Region Pressed. IGNORE!!!\n" );
				}
				else
				{
				#endif // 20101215 seven@lge.com grip suppression [start]

					if ((((ts_reg_data.fingers_data[i][3] & 0xf0) >> 4) - (ts_reg_data.fingers_data[i][3] & 0x0f)) > 0)
						curr_ts_data.width[i] = (ts_reg_data.fingers_data[i][3] & 0xf0) >> 4;
					else
						curr_ts_data.width[i] = ts_reg_data.fingers_data[i][3] & 0x0f;

					curr_ts_data.pressure[i] = ts_reg_data.fingers_data[i][4];

					curr_ts_data.touch_status[i] = 1;

					finger_count++;
				}
			}
			else
			{
				curr_ts_data.touch_status[i] = 0;
			}
		}

		for(i = 0; i < MAX_NUM_OF_FINGER; i++)
		{
			if(curr_ts_data.touch_status[i])
			{
				if(finger_count == 1 && i == 0)
				{
					if((curr_ts_data.Y_position[i] < SYNAPTICS_PANEL_LCD_MAX_Y && prev_event_type == TOUCH_EVENT_NULL) || prev_event_type == TOUCH_EVENT_ABS)
						curr_event_type = TOUCH_EVENT_ABS;
					else if((curr_ts_data.Y_position[i] >= SYNAPTICS_PANEL_LCD_MAX_Y && prev_event_type == TOUCH_EVENT_NULL) || prev_event_type == TOUCH_EVENT_BUTTON)
						curr_event_type = TOUCH_EVENT_BUTTON;

					if(curr_event_type == TOUCH_EVENT_ABS)
					{
						if(curr_ts_data.Y_position[i] < SYNAPTICS_PANEL_LCD_MAX_Y)
						{
							input_report_abs(ts->input_dev, ABS_MT_POSITION_X, curr_ts_data.X_position[i]);
							input_report_abs(ts->input_dev, ABS_MT_POSITION_Y, curr_ts_data.Y_position[i]);
							input_report_abs(ts->input_dev, ABS_MT_TOUCH_MAJOR, curr_ts_data.pressure[i]);
							input_report_abs(ts->input_dev, ABS_MT_WIDTH_MAJOR, curr_ts_data.width[i]);

							input_mt_sync(ts->input_dev);
//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time. [START]
							sent_event[i] = 1;
//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time. [END]

							pr_debug("[TOUCH-1] (X, Y) = (%d, %d), z = %d, w = %d\n", curr_ts_data.X_position[i], curr_ts_data.Y_position[i], curr_ts_data.pressure[i], curr_ts_data.width[i]);
							//printk("[TOUCH-1] (X, Y) = (%d, %d), z = %d, w = %d\n", curr_ts_data.X_position[i], curr_ts_data.Y_position[i], curr_ts_data.pressure[i], curr_ts_data.width[i]);
						}
					}
					else if(curr_event_type == TOUCH_EVENT_BUTTON)
					{
						if(curr_ts_data.Y_position[i] > SYNAPTICS_PANEL_BUTTON_MIN_Y)
						{
							if(curr_ts_data.X_position[i] > 107 && curr_ts_data.X_position[i] < 317) //center 75
							{
								if(!prev_ts_data.touch_status[i])
								{
									input_report_key(ts->input_dev, KEY_MENU, 1); //seven blocked for key drag action
									pr_debug("[TOUCH-2] Key Event KEY = %d, PRESS = %d\n", KEY_MENU, 1);
									pressed_button_type = KEY_MENU;
								}
								else
								{
									if(pressed_button_type != KEY_MENU && pressed_button_type != KEY_REJECT)
									{
										input_report_key(ts->input_dev, KEY_REJECT, 1);
										pr_debug("[TOUCH-3] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 1);
										input_report_key(ts->input_dev, KEY_REJECT, 0);
										pr_debug("[TOUCH] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 0);
										input_report_key(ts->input_dev, pressed_button_type, 0);
										pr_debug("[TOUCH-4] Key Event KEY = %d, PRESS = %d\n", pressed_button_type, 0);
										pressed_button_type = KEY_REJECT;
									}
								}
							}
							else if(curr_ts_data.X_position[i] > 469 && curr_ts_data.X_position[i] < 679) //center 185
							{
								if(!prev_ts_data.touch_status[i])
								{
									input_report_key(ts->input_dev, KEY_HOME, 1); //seven blocked for key drag action
									pr_debug("[TOUCH-5] Key Event KEY = %d, PRESS = %d\n", KEY_HOME, 1);
									pressed_button_type = KEY_HOME;
								}
								else
								{
									if(pressed_button_type != KEY_HOME && pressed_button_type != KEY_REJECT)
									{
										input_report_key(ts->input_dev, KEY_REJECT, 1);
										pr_debug("[TOUCH-6] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 1);
										input_report_key(ts->input_dev, KEY_REJECT, 0);
										pr_debug("[TOUCH-6] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 0);
										input_report_key(ts->input_dev, pressed_button_type, 0);
										pr_debug("[TOUCH-8] Key Event KEY = %d, PRESS = %d\n", pressed_button_type, 0);
										pressed_button_type = KEY_REJECT;
									}
								}
							}
							else if(curr_ts_data.X_position[i] > 893 && curr_ts_data.X_position[i] < 1013) //center 295
							{
								if(!prev_ts_data.touch_status[i])
								{
									input_report_key(ts->input_dev, KEY_BACK, 1); //seven blocked for key drag action
									pr_debug("[TOUCH-9] Key Event KEY = %d, PRESS = %d\n", KEY_BACK, 1);
									pressed_button_type = KEY_BACK;
								}
								else
								{
									if(pressed_button_type != KEY_BACK && pressed_button_type != KEY_REJECT)
									{
										input_report_key(ts->input_dev, KEY_REJECT, 1);
										pr_debug("[TOUCH-10] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 1);
										input_report_key(ts->input_dev, KEY_REJECT, 0);
										pr_debug("[TOUCH-11] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 0);
										input_report_key(ts->input_dev, pressed_button_type, 0);
										pr_debug("[TOUCH-12] Key Event KEY = %d, PRESS = %d\n", pressed_button_type, 0);
										pressed_button_type = KEY_REJECT;
									}
								}
							}
							else if(curr_ts_data.X_position[i] > 1253 && curr_ts_data.X_position[i] < 1467) //center 405
							{
								if(!prev_ts_data.touch_status[i])
								{
									input_report_key(ts->input_dev, KEY_SEARCH, 1); //seven blocked for key drag action
									pr_debug("[TOUCH-13] Key Event KEY = %d, PRESS = %d\n", KEY_SEARCH, 1);
									pressed_button_type = KEY_SEARCH;
								}
								else
								{
									if(pressed_button_type != KEY_SEARCH && pressed_button_type != KEY_REJECT)
									{
										input_report_key(ts->input_dev, KEY_REJECT, 1);
										pr_debug("[TOUCH-14] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 1);
										input_report_key(ts->input_dev, KEY_REJECT, 0);
										pr_debug("[TOUCH-15] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 0);
										input_report_key(ts->input_dev, pressed_button_type, 0);
										pr_debug("[TOUCH-16] Key Event KEY = %d, PRESS = %d\n", pressed_button_type, 0);
										pressed_button_type = KEY_REJECT;
									}
								}
							}
							else
							{
								if(!prev_ts_data.touch_status[i])
								{
									pressed_button_type = KEY_REJECT;
								}
								else
								{
									if(pressed_button_type != KEY_REJECT)
									{
										input_report_key(ts->input_dev, KEY_REJECT, 1);
										pr_debug("[TOUCH-17] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 1);
										input_report_key(ts->input_dev, KEY_REJECT, 0);
										pr_debug("[TOUCH-18] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 0);
										input_report_key(ts->input_dev, pressed_button_type, 0);
										pr_debug("[TOUCH-19] Key Event KEY = %d, PRESS = %d\n", pressed_button_type, 0);
										pressed_button_type = KEY_REJECT;
									}
								}
							}

						}
						else
						{
							if(!prev_ts_data.touch_status[i])
							{
								pressed_button_type = KEY_REJECT;
							}
							else
							{
								if(pressed_button_type != KEY_REJECT)
								{
									input_report_key(ts->input_dev, KEY_REJECT, 1);
									pr_debug("[TOUCH-20] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 1);
									input_report_key(ts->input_dev, KEY_REJECT, 0);
									pr_debug("[TOUCH-21] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 0);
									input_report_key(ts->input_dev, pressed_button_type, 0);
									pr_debug("[TOUCH-22] Key Event KEY = %d, PRESS = %d\n", pressed_button_type, 0);
									pressed_button_type = KEY_REJECT;
								}
							}

// 20101021 joseph.jung@lge.com lcd-button area touch scenario change [START]
								input_report_abs(ts->input_dev, ABS_MT_POSITION_X, curr_ts_data.X_position[i]);
								input_report_abs(ts->input_dev, ABS_MT_POSITION_Y, curr_ts_data.Y_position[i]);
								input_report_abs(ts->input_dev, ABS_MT_TOUCH_MAJOR, curr_ts_data.pressure[i]);
								input_report_abs(ts->input_dev, ABS_MT_WIDTH_MAJOR, curr_ts_data.width[i]);

								input_mt_sync(ts->input_dev);
//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time. [START]
								sent_event[i] = 1;
//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time. [END]
								pr_debug("[TOUCH-23] (X, Y) = (%d, %d), z = %d, w = %d\n", curr_ts_data.X_position[i], curr_ts_data.Y_position[i], curr_ts_data.pressure[i], curr_ts_data.width[i]);

								curr_event_type = TOUCH_EVENT_ABS;
// 20101021 joseph.jung@lge.com lcd-button area touch scenario change [END]
						}
					}
					else
					{
						curr_event_type = TOUCH_EVENT_NULL;
						pressed_button_type = KEY_REJECT;
					}
					//break; /* 20101231 seven.kim@lge.com do not need any more */
				}
				else // multi-finger
				{
					curr_event_type = TOUCH_EVENT_ABS;

					if(pressed_button_type != KEY_REJECT)
					{
						input_report_key(ts->input_dev, KEY_REJECT, 1);
						pr_debug("[TOUCH-24] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 1);
						input_report_key(ts->input_dev, KEY_REJECT, 0);
						pr_debug("[TOUCH-25] Key Event KEY = %d, PRESS = %d\n", KEY_REJECT, 0);
						input_report_key(ts->input_dev, pressed_button_type, 0);
						pr_debug("[TOUCH-26] Key Event KEY = %d, PRESS = %d\n", pressed_button_type, 0);
						pressed_button_type = KEY_REJECT;
					}

					if(curr_ts_data.Y_position[i] < SYNAPTICS_PANEL_LCD_MAX_Y)
					{
						input_report_abs(ts->input_dev, ABS_MT_POSITION_X, curr_ts_data.X_position[i]);
						input_report_abs(ts->input_dev, ABS_MT_POSITION_Y, curr_ts_data.Y_position[i]);
						input_report_abs(ts->input_dev, ABS_MT_TOUCH_MAJOR, curr_ts_data.pressure[i]);
						input_report_abs(ts->input_dev, ABS_MT_WIDTH_MAJOR, curr_ts_data.width[i]);

						input_mt_sync(ts->input_dev);

						/*20110227 seven.kim@lge.com to remove ghost finger for HW Drop Test [START] */
						if(curr_ts_data.X_position[1] || curr_ts_data.Y_position[1])
							//ghost_count=0;
						/*20110227 seven.kim@lge.com to remove ghost finger for HW Drop Test [START] */
//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time. [START]
						sent_event[i] = 1;
//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time. [END]

						pr_debug("[TOUCH-27] (X, Y) = (%d, %d), z = %d, w = %d\n", curr_ts_data.X_position[i], curr_ts_data.Y_position[i], curr_ts_data.pressure[i], curr_ts_data.width[i]);
					}
				}
			}
			else
			{
				if(pressed_button_type != KEY_REJECT && i == 0)
				{
					input_report_key(ts->input_dev, pressed_button_type, 0);

					pr_debug("[TOUCH-28] Key Event KEY = %d, PRESS = %d\n", pressed_button_type, 0);
					pressed_button_type = KEY_REJECT;
				}
//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time. [START]
				if(sent_event[i] == 1)
				{
					curr_ts_data.X_position[i] = (int)TS_SNTS_GET_X_POSITION(ts_reg_data.fingers_data[i][0], ts_reg_data.fingers_data[i][2]);
					curr_ts_data.Y_position[i] = (int)TS_SNTS_GET_Y_POSITION(ts_reg_data.fingers_data[i][1], ts_reg_data.fingers_data[i][2]);

					input_report_abs(ts->input_dev, ABS_MT_POSITION_X, curr_ts_data.X_position[i]);
					input_report_abs(ts->input_dev, ABS_MT_POSITION_Y, curr_ts_data.Y_position[i]);
					input_report_abs(ts->input_dev, ABS_MT_TOUCH_MAJOR, 0);
					input_report_abs(ts->input_dev, ABS_MT_WIDTH_MAJOR, 0);
					input_mt_sync(ts->input_dev);
					sent_event[i] = 0;

					printk("release touch event \n");
				}
//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time. [END]
			}
		}

		if(finger_count == 0)
		{
			prev_event_type = TOUCH_EVENT_NULL;
		}
		else
		{
			prev_event_type = curr_event_type;
		}

	}
//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time. [START]
#if 1
	input_sync(ts->input_dev);
#else
	input_mt_sync(ts->input_dev);
	input_sync(ts->input_dev);
#endif
//20110501 yongman.kwon@lge.com [LS855] bug fix : sending touch event twice a one time. [END]

/*[START] seven.kim@lge.com To avoid touch sensor lock-up & data missing in touch release*/
}while(Synaptics_Check_Touch_Interrupt_Status());
/*[END] seven.kim@lge.com To avoid touch sensor lock-up & data missing in touch release*/
//SYNAPTICS_TS_IDLE:
	if (ts->use_irq) {
		enable_irq(ts->client->irq);
	}
}

static enum hrtimer_restart synaptics_ts_timer_func(struct hrtimer *timer)
{
	struct synaptics_ts_data *ts = container_of(timer, struct synaptics_ts_data, timer);

	queue_delayed_work(synaptics_wq, &ts->work, 0);
	hrtimer_start(&ts->timer, ktime_set(0, ts->pdata->report_period), HRTIMER_MODE_REL); /* 12.5 msec */

	return HRTIMER_NORESTART;
}

static irqreturn_t synaptics_ts_thread_irq_handler(int irq, void *dev_id)
{
	struct synaptics_ts_data *ts = dev_id;

	//pr_info("LGE: synaptics_ts_irq_handler\n");
	disable_irq_nosync(ts->client->irq);

	queue_delayed_work(synaptics_wq, &ts->work, 0);

	return IRQ_HANDLED;
}

/*
static unsigned char synaptics_ts_check_fwver(struct i2c_client *client)
{
	unsigned char RMI_Query_BaseAddr;
	unsigned char FWVersion_Addr;

	unsigned char SynapticsFirmVersion;

	RMI_Query_BaseAddr = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_RMI_QUERY_BASE_REG);
	FWVersion_Addr = RMI_Query_BaseAddr+3;

	SynapticsFirmVersion = synaptics_ts_i2c_read_byte_data(client, FWVersion_Addr);
	pr_info("[TOUCH] Touch controller Firmware Version = %x\n", SynapticsFirmVersion);

	return SynapticsFirmVersion;
}

*/

#ifdef SYNAPTICS_SUPPORT_FW_UPGRADE
static unsigned long ExtractLongFromHeader(const unsigned char *SynaImage)  // Endian agnostic
{
  return((unsigned long)SynaImage[0] +
         (unsigned long)SynaImage[1]*0x100 +
         (unsigned long)SynaImage[2]*0x10000 +
         (unsigned long)SynaImage[3]*0x1000000);
}

static void CalculateChecksum(uint16_t *data, uint16_t len, uint32_t *dataBlock)
{
  unsigned long temp = *data++;
  unsigned long sum1;
  unsigned long sum2;

  *dataBlock = 0xffffffff;

  sum1 = *dataBlock & 0xFFFF;
  sum2 = *dataBlock >> 16;

  while (len--)
  {
    sum1 += temp;
    sum2 += sum1;
    sum1 = (sum1 & 0xffff) + (sum1 >> 16);
    sum2 = (sum2 & 0xffff) + (sum2 >> 16);
  }

  *dataBlock = sum2 << 16 | sum1;
}

static void SpecialCopyEndianAgnostic(uint8_t *dest, uint16_t src)
{
  dest[0] = src%0x100;  //Endian agnostic method
  dest[1] = src/0x100;
}


static bool synaptics_ts_fw_upgrade(struct i2c_client *client)
{
	struct synaptics_ts_data *ts = i2c_get_clientdata(client);
	int i;
	int j;

	uint8_t FlashQueryBaseAddr, FlashDataBaseAddr;
	uint8_t RMICommandBaseAddr;

	uint8_t BootloaderIDAddr;
	uint8_t BlockSizeAddr;
	uint8_t FirmwareBlockCountAddr;
	uint8_t ConfigBlockCountAddr;

	uint8_t BlockNumAddr;
	uint8_t BlockDataStartAddr;

	uint8_t current_fw_ver;

	uint8_t bootloader_id[2];

	uint8_t temp_array[2], temp_data, flashValue, m_firmwareImgVersion;
	uint8_t checkSumCode;

	uint16_t ts_block_size, ts_config_block_count, ts_fw_block_count;
	uint16_t m_bootloadImgID;

	uint32_t ts_config_img_size;
	uint32_t ts_fw_img_size;
	uint32_t pinValue, m_fileSize, m_firmwareImgSize, m_configImgSize, m_FirmwareImgFile_checkSum;

	////////////////////////////

	pr_debug("[Touch Driver] Synaptics_UpgradeFirmware [START]\n");
/*
	if(!(synaptics_ts_check_fwver(client) < SynapticsFirmware[0x1F]))
	{
		// Firmware Upgrade does not necessary!!!!
		pr_debug("[Touch Driver] Synaptics_UpgradeFirmware does not necessary!!!!\n");
		return true;
	}
*/
	if (ts->product_value==1)
	{
		memcpy(SynapticsFirmware, SynapticsFirmware_misung, sizeof(SynapticsFirmware_misung));
		current_fw_ver = synaptics_ts_check_fwver(client);
		if((current_fw_ver >= 0x64 && SynapticsFirmware[0x1F] >= 0x64) || (current_fw_ver < 0x64 && SynapticsFirmware[0x1F] < 0x64))
		{
			if(!(current_fw_ver < SynapticsFirmware[0x1F]))
			{
				// Firmware Upgrade does not necessary!!!!
				pr_debug("[Touch Driver] Synaptics_UpgradeFirmware does not necessary!!!!\n");
				return true;
			}
		}
	}
	else if (ts->product_value==2)
	{
			memcpy(SynapticsFirmware, SynapticsFirmware_lgit, sizeof(SynapticsFirmware_lgit));
			current_fw_ver = synaptics_ts_check_fwver(client);
			if((current_fw_ver >= 0x01 && SynapticsFirmware[0x1F] >= 0x01) || (current_fw_ver < 0x01 && SynapticsFirmware[0x1F] < 0x01))
			{
				if(!(current_fw_ver < SynapticsFirmware[0x1F]))
				{
					// Firmware Upgrade does not necessary!!!!
					pr_debug("[Touch Driver] Synaptics_UpgradeFirmware does not necessary!!!!\n");
					return true;
				}
			}
	}
	else
		return true;

	// Address Configuration
	FlashQueryBaseAddr = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_FLASH_QUERY_BASE_REG);

	BootloaderIDAddr = FlashQueryBaseAddr;
	BlockSizeAddr = FlashQueryBaseAddr + 3;
	FirmwareBlockCountAddr = FlashQueryBaseAddr + 5;
	ConfigBlockCountAddr = FlashQueryBaseAddr + 7;


	FlashDataBaseAddr = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_FLASH_DATA_BASE_REG);

	BlockNumAddr = FlashDataBaseAddr;
	BlockDataStartAddr = FlashDataBaseAddr + 2;

	// Get New Firmware Information from Header
	m_fileSize = sizeof(SynapticsFirmware) -1;

	checkSumCode         = ExtractLongFromHeader(&(SynapticsFirmware[0]));
	m_bootloadImgID      = (unsigned int)SynapticsFirmware[4] + (unsigned int)SynapticsFirmware[5]*0x100;
	m_firmwareImgVersion = SynapticsFirmware[7];
	m_firmwareImgSize    = ExtractLongFromHeader(&(SynapticsFirmware[8]));
	m_configImgSize      = ExtractLongFromHeader(&(SynapticsFirmware[12]));

	CalculateChecksum((uint16_t*)&(SynapticsFirmware[4]), (uint16_t)(m_fileSize-4)>>1, &m_FirmwareImgFile_checkSum);

	// Get Current Firmware Information
	synaptics_ts_i2c_read_block_data(client, BlockSizeAddr, sizeof(temp_array), (u8 *)&temp_array[0]);
	ts_block_size = temp_array[0] + (temp_array[1] << 8);

	synaptics_ts_i2c_read_block_data(client, FirmwareBlockCountAddr, sizeof(temp_array), (u8 *)&temp_array[0]);
	ts_fw_block_count = temp_array[0] + (temp_array[1] << 8);
	ts_fw_img_size = ts_block_size * ts_fw_block_count;

	synaptics_ts_i2c_read_block_data(client, ConfigBlockCountAddr, sizeof(temp_array), (u8 *)&temp_array[0]);
	ts_config_block_count = temp_array[0] + (temp_array[1] << 8);
	ts_config_img_size = ts_block_size * ts_config_block_count;

	synaptics_ts_i2c_read_block_data(client, BootloaderIDAddr, sizeof(bootloader_id), (u8 *)&bootloader_id[0]);
	pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: BootloaderID %02x %02x\n", bootloader_id[0], bootloader_id[1]);

	// Compare
	if (m_fileSize != (0x100+m_firmwareImgSize+m_configImgSize))
	{
		pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: Error : Invalid FileSize\n");
		return true;
	}

	if (m_firmwareImgSize != ts_fw_img_size)
	{
		pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: Error : Invalid Firmware Image Size\n");
		return true;
	}

	if (m_configImgSize != ts_config_img_size)
	{
		pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: Error : Invalid Config Image Size\n");
		return true;
	}

	// Flash Write Ready - Flash Command Enable & Erase
	//synaptics_ts_i2c_write_block_data(client, BlockDataStartAddr, sizeof(bootloader_id), &bootloader_id[0]);
	// How can i use 'synaptics_ts_i2c_write_block_data'
	for(i = 0; i < sizeof(bootloader_id); i++)
	{
		if(synaptics_ts_i2c_write_byte_data(client, BlockDataStartAddr+i, bootloader_id[i]))
			pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: Address %02x, Value %02x\n", BlockDataStartAddr+i, bootloader_id[i]);
	}

	do
	{
		flashValue = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG);
		temp_data = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_INT_STATUS_REG);
	} while((flashValue & 0x0f) != 0x00);

	synaptics_ts_i2c_write_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG, SYNAPTICS_FLASH_CMD_ENABLE);

	do
	{
		pinValue = gpio_get_value(TOUCH_INT_N_GPIO);
		mdelay(1);
	} while(pinValue);
	do
	{
		flashValue = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG);
		temp_data = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_INT_STATUS_REG);
	} while(flashValue != 0x80);
	flashValue = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG);

	pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: Flash Program Enable Setup Complete\n");

	//synaptics_ts_i2c_write_block_data(client, BlockDataStartAddr, sizeof(bootloader_id), &bootloader_id[0]);
	// How can i use 'synaptics_ts_i2c_write_block_data'
	for(i = 0; i < sizeof(bootloader_id); i++)
	{
		if(synaptics_ts_i2c_write_byte_data(client, BlockDataStartAddr+i, bootloader_id[i]))
			pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: Address %02x, Value %02x\n", BlockDataStartAddr+i, bootloader_id[i]);
	}

	if(m_firmwareImgVersion == 0 && ((unsigned int)bootloader_id[0] + (unsigned int)bootloader_id[1]*0x100) != m_bootloadImgID)
	{
		pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: Error : Invalid Bootload Image\n");
		return true;
	}

	synaptics_ts_i2c_write_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG, SYNAPTICS_FLASH_CMD_ERASEALL);

	pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: SYNAPTICS_FLASH_CMD_ERASEALL\n");

	do
	{
		pinValue = gpio_get_value(TOUCH_INT_N_GPIO);
		mdelay(1);
	} while(pinValue);
	do
	{
		flashValue = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG);
		temp_data = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_INT_STATUS_REG);
	} while(flashValue != 0x80);

	pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: Flash Erase Complete\n");

	// Flash Firmware Data Write
	for(i = 0; i < ts_fw_block_count; ++i)
	{
		temp_array[0] = i & 0xff;
		temp_array[1] = (i & 0xff00) >> 8;

		// Write Block Number
		//synaptics_ts_i2c_write_block_data(client, BlockNumAddr, sizeof(temp_array), &temp_array[0]);
		// How can i use 'synaptics_ts_i2c_write_block_data'
		for(j = 0; j < sizeof(temp_array); j++)
		{
			synaptics_ts_i2c_write_byte_data(client, BlockNumAddr+j, temp_array[j]);
		}

		// Write Data Block&SynapticsFirmware[0]
		//synaptics_ts_i2c_write_block_data(client, BlockDataStartAddr, ts_block_size, &SynapticsFirmware[0x100+i*ts_block_size]);
		// How can i use 'synaptics_ts_i2c_write_block_data'
		for(j = 0; j < ts_block_size; j++)
		{
			synaptics_ts_i2c_write_byte_data(client, BlockDataStartAddr+j, SynapticsFirmware[0x100+i*ts_block_size+j]);
		}

		// Issue Write Firmware Block command
		synaptics_ts_i2c_write_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG, SYNAPTICS_FLASH_CMD_FW_WRITE);
		do
		{
			pinValue = gpio_get_value(TOUCH_INT_N_GPIO);
			mdelay(1);
		} while(pinValue);
		do
		{
			flashValue = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG);
			temp_data = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_INT_STATUS_REG);
		} while(flashValue != 0x80);
	} //for

	pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: Flash Firmware Write Complete\n");

	// Flash Firmware Config Write
	for(i = 0; i < ts_config_block_count; i++)
	{
		SpecialCopyEndianAgnostic(&temp_array[0], i);

		// Write Configuration Block Number
		synaptics_ts_i2c_write_block_data(client, BlockNumAddr, sizeof(temp_array), &temp_array[0]);
		// How can i use 'synaptics_ts_i2c_write_block_data'
		for(j = 0; j < sizeof(temp_array); j++)
		{
			synaptics_ts_i2c_write_byte_data(client, BlockNumAddr+j, temp_array[j]);
		}

		// Write Data Block
		//synaptics_ts_i2c_write_block_data(client, BlockDataStartAddr, ts_block_size, &SynapticsFirmware[0x100+m_firmwareImgSize+i*ts_block_size]);
		// How can i use 'synaptics_ts_i2c_write_block_data'
		for(j = 0; j < ts_block_size; j++)
		{
			synaptics_ts_i2c_write_byte_data(client, BlockDataStartAddr+j, SynapticsFirmware[0x100+m_firmwareImgSize+i*ts_block_size+j]);
		}

		// Issue Write Configuration Block command to flash command register
		synaptics_ts_i2c_write_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG, SYNAPTICS_FLASH_CMD_CONFIG_WRITE);
		do
		{
			pinValue = gpio_get_value(TOUCH_INT_N_GPIO);
			mdelay(1);
		} while(pinValue);
		do
		{
			flashValue = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG);
			temp_data = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_INT_STATUS_REG);
		} while(flashValue != 0x80);
	}

	pr_debug("[TOUCH] Synaptics_UpgradeFirmware :: Flash Config Write Complete\n");


	RMICommandBaseAddr = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_RMI_CMD_BASE_REG);
	synaptics_ts_i2c_write_byte_data(client, RMICommandBaseAddr, 0x01);
	mdelay(100);

	do
	{
		pinValue = gpio_get_value(TOUCH_INT_N_GPIO);
		mdelay(1);
	} while(pinValue);
	do
	{
		flashValue = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_FLASH_CONTROL_REG);
		temp_data = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_INT_STATUS_REG);
	} while((flashValue & 0x0f) != 0x00);

	// Clear the attention assertion by reading the interrupt status register
	temp_data = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_INT_STATUS_REG);

	// Read F01 Status flash prog, ensure the 6th bit is '0'
	do
	{
		temp_data = synaptics_ts_i2c_read_byte_data(client, SYNAPTICS_DATA_BASE_REG);
	} while((temp_data & 0x40) != 0);

	return true;
}
#endif
static struct synaptics_ts_data *touch_pdev = NULL;
#if 1 // test_mode command
void Send_Touch( unsigned int x, unsigned int y)
{
	if(touch_pdev)
	{

		input_report_abs(touch_pdev->input_dev, ABS_MT_POSITION_X, x);
		input_report_abs(touch_pdev->input_dev, ABS_MT_POSITION_Y, y);
		input_report_abs(touch_pdev->input_dev, ABS_MT_PRESSURE, 1);
		input_report_abs(touch_pdev->input_dev, ABS_MT_WIDTH_MAJOR, 1);
		input_report_abs(touch_pdev->input_dev, ABS_MT_WIDTH_MINOR, 1);
		input_mt_sync(touch_pdev->input_dev);
		input_sync(touch_pdev->input_dev);

		input_report_abs(touch_pdev->input_dev, ABS_MT_POSITION_X, x);
		input_report_abs(touch_pdev->input_dev, ABS_MT_POSITION_Y, y);
		input_report_abs(touch_pdev->input_dev, ABS_MT_PRESSURE, 0);
		input_report_abs(touch_pdev->input_dev, ABS_MT_WIDTH_MAJOR, 0);
		input_report_abs(touch_pdev->input_dev, ABS_MT_WIDTH_MINOR, 0);
		input_mt_sync(touch_pdev->input_dev);
		input_sync(touch_pdev->input_dev);
	}
	else
	{
		pr_debug("synaptics_ts_data not found\n");
	}
}
EXPORT_SYMBOL(Send_Touch);

int get_touch_ts_fw_version(char *fw_ver)
{
	if(touch_pdev)
	{
		sprintf(fw_ver, "%s.0%d", "Synaptics", touch_pdev->fw_rev);
		return 1;
	}
	else
	{
		return 0;
	}
}

EXPORT_SYMBOL(get_touch_ts_fw_version);
#endif


#ifdef FEATURE_LGE_TOUCH_ESD_DETECT //20101221 seven.kim@lge.com to detect Register change by ESD
static void Synatics_ts_touch_Recovery(struct synaptics_ts_data *ts)
{

	int ret = 0;

       if (ts->pdata->power_enable){
			ret = ts->pdata->power_enable(0, true);

               msleep(50);

			ret = ts->pdata->power_enable(1, true);

       }
}

#endif /*FEATURE_LGE_TOUCH_ESD_DETECT*/

static void synaptics_ts_dev_init(int qpio_num)
{
	int rc;
	rc = gpio_request(qpio_num, "touch_int");
	if (rc < 0) {
		pr_err("Can't get synaptics pen down GPIO\n");
		return ;
	}
	gpio_direction_input(qpio_num);
	gpio_set_value(qpio_num, 1);
}

/*************************************************************************************************
 * 1. Set interrupt configuration
 * 2. Disable interrupt
 * 3. Power up
 * 4. Read RMI Version
 * 5. Read Firmware version & Upgrade firmware automatically
 * 6. Read Data To Initialization Touch IC
 * 7. Set some register
 * 8. Enable interrupt
*************************************************************************************************/
static int synaptics_ts_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct synaptics_ts_platform_data *pdata;
	struct synaptics_ts_data *ts;
	int ret = 0;
	uint16_t max_x;
	uint16_t max_y;
	uint8_t max_pressure;
	uint8_t max_width;

#ifdef FEATURE_LGE_TOUCH_REAL_TIME_WORK_QUEUE	//20101221 seven.kim@lge.com to use real time work queue
		synaptics_wq = create_rt_workqueue("synaptics_wq");
#else
		synaptics_wq = create_singlethread_workqueue("synaptics_wq");
#endif /*FEATURE_LGE_TOUCH_REAL_TIME_WORK_QUEUE*/

		pr_warning("LGE: Synaptics ts_init\n");
		if (!synaptics_wq)
			return -ENOMEM;


	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		pr_err("synaptics_ts_probe: need I2C_FUNC_I2C\n");
		ret = -ENODEV;
		goto err_check_functionality_failed;
	}

    pdata = client->dev.platform_data;

	if (pdata == NULL) {
		pr_err("Can not read platform data\n");
		ret = -ENODEV;
		goto err_check_functionality_failed;
	}
	touch_pdev = ts = kzalloc(sizeof(*ts), GFP_KERNEL);
	if (ts == NULL) {
		ret = -ENOMEM;
		goto err_alloc_data_failed;
	}

	ts->pdata = pdata;
	INIT_DELAYED_WORK(&ts->work, synaptics_ts_work_func);
	INIT_DELAYED_WORK(&ts->init_delayed_work, synaptics_ts_init_delayed_work);

	ts->client = client;
	hub_ts_client = client;
	i2c_set_clientdata(client, ts);

	if (ts->pdata->power_enable){

		 ret = ts->pdata->power_enable(1,true);
		 		if (ret < 0) {
					pr_debug("Touch power on failed\n");
		 			}

	}
	msleep(50);
	synaptics_ts_dev_init(ts->pdata->i2c_int_gpio);

#ifdef FEATURE_LGE_TOUCH_ESD_DETECT //20101221 seven.kim@lge.com to detect Register change by ESD
	wake_lock_init(&ts_wake_lock, WAKE_LOCK_SUSPEND, "ts_upgrade");
#endif /*FEATURE_LGE_TOUCH_ESD_DETECT*/

// 20101215 seven.kim@lge.com grip suppression [START]
#ifdef FEATURE_LGE_TOUCH_GRIP_SUPPRESSION
	ret = device_create_file(&client->dev, &dev_attr_gripsuppression);
	if (ret) {
		pr_err("synaptics_ts_probe: grip suppression device_create_file failed\n");
		goto err_check_functionality_failed;
	}
#endif /* FEATURE_LGE_TOUCH_GRIP_SUPPRESSION */
// 20101215 seven.kim@lge.com grip suppression [END]
/*
	mdelay(50);

	touch_fw_version=synaptics_ts_check_fwver(hub_ts_client);

	pr_info("touch_fw_version : %d\n", touch_fw_version);

  	memset(&ts_reg_data, 0x0, sizeof(ts_sensor_data));
	memset(&prev_ts_data, 0x0, sizeof(ts_finger_data));
  	memset(&curr_ts_data, 0x0, sizeof(ts_finger_data));
*/


	/*************************************************************************************************
	 * 3. Power up
	 *************************************************************************************************/
	/* It's controlled by LCD BL CHARGER PUMP */


	/*************************************************************************************************
	 * 4. Read RMI Version
	 * To distinguish T1021 and T1007. Select RMI Version
	 * TODO: Power를 이전에 하는 것으로 변경하면 위치 변경해야 한다.
	 *************************************************************************************************/


	SYNAPTICS_PANEL_MAX_X = SYNAPTICS_TM1940_RESOLUTION_X;
	SYNAPTICS_PANEL_MAX_Y = SYNAPTICS_TM1940_RESOLUTION_Y;
	SYNAPTICS_PANEL_LCD_MAX_Y = SYNAPTICS_TM1940_LCD_ACTIVE_AREA;
	SYNAPTICS_PANEL_BUTTON_MIN_Y = SYNAPTICS_TM1940_BUTTON_ACTIVE_AREA;


	ts->input_dev = input_allocate_device();
	if (ts->input_dev == NULL) {
		ret = -ENOMEM;
		pr_err("synaptics_ts_probe: Failed to allocate input device\n");
		goto err_input_dev_alloc_failed;
	}
	ts->input_dev->name = "synaptics_ts";

	set_bit(EV_SYN, ts->input_dev->evbit);
	set_bit(EV_KEY, ts->input_dev->evbit);
	set_bit(BTN_TOUCH, ts->input_dev->keybit);
	set_bit(EV_ABS, ts->input_dev->evbit);

	// button
	set_bit(KEY_MENU, ts->input_dev->keybit);
	set_bit(KEY_HOME, ts->input_dev->keybit);
	set_bit(KEY_BACK, ts->input_dev->keybit);
	set_bit(KEY_SEARCH, ts->input_dev->keybit);
	set_bit(KEY_REJECT, ts->input_dev->keybit);

	max_x = SYNAPTICS_PANEL_MAX_X;
	max_y = SYNAPTICS_PANEL_LCD_MAX_Y;
	max_pressure = 0xFF;
	max_width = 0x0F;

	input_set_abs_params(ts->input_dev, ABS_MT_POSITION_X, 0, max_x, 0, 0);
	input_set_abs_params(ts->input_dev, ABS_MT_POSITION_Y, 0, max_y, 0, 0);
	input_set_abs_params(ts->input_dev, ABS_MT_TOUCH_MAJOR, 0, max_pressure, 0, 0);
	input_set_abs_params(ts->input_dev, ABS_MT_WIDTH_MAJOR, 0, max_width, 0, 0);

	pr_info("synaptics_ts_probe: max_x %d, max_y %d\n", max_x, max_y);


	/* ts->input_dev->name = ts->keypad_info->name; */
	ret = input_register_device(ts->input_dev);
	if (ret) {
		pr_err("synaptics_ts_probe: Unable to register %s input device\n", ts->input_dev->name);
		goto err_input_register_device_failed;
	}

	pr_info("########## irq [%d], irqflags[0x%x]\n", client->irq, IRQF_TRIGGER_FALLING);

	//[START] 20101227 seven.kim@lge.com to prevent interrupt in booting time
	synaptics_ts_i2c_write_byte_data(hub_ts_client, SYNAPTICS_RIM_CONTROL_INTERRUPT_ENABLE, 0x00); //interrupt disable
	//[START] 20101227 seven.kim@lge.com to prevent interrupt in booting time

	if (client->irq) {
		ret = request_threaded_irq(client->irq, NULL, synaptics_ts_thread_irq_handler,
						ts->pdata->irqflags | IRQF_ONESHOT, client->name, ts);

		if (ret == 0) {
			ts->use_irq = 1;
			pr_warning("Touch request_irq\n");
			}
		else
			dev_err(&client->dev, "request_irq failed\n");
	}
	if (!ts->use_irq) {
		hrtimer_init(&ts->timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
		ts->timer.function = synaptics_ts_timer_func;
		hrtimer_start(&ts->timer,
					ktime_set(0, (ts->pdata->report_period * 2) + (ts->pdata->ic_booting_delay*1000000)),
					HRTIMER_MODE_REL);
	}



// 20100826 jh.koo@lge.com, for stable initialization [START_LGE]
	schedule_delayed_work(&ts->init_delayed_work, msecs_to_jiffies(200/*500*/));
// 20100826 jh.koo@lge.com, for stable initialization [END_LGE]

	/* 20110216 seven.kim@lge.com to follow android sleep/resume flow [START] */
//	atomic_set( &g_synaptics_ts_resume_flag, 0 );
//	atomic_set( &g_synaptics_ts_suspend_flag, 0 );
	/* 20110216 seven.kim@lge.com to follow android sleep/resume flow [END] */

#ifdef CONFIG_HAS_EARLYSUSPEND /*20110214 to follow Android Resume flow for LCD */
	ts->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN + 1;
	ts->early_suspend.suspend = synaptics_ts_early_suspend;
	ts->early_suspend.resume = synaptics_ts_late_resume;
	register_early_suspend(&ts->early_suspend);
#endif

	pr_warning("synaptics_ts_probe: Start touchscreen %s in %s mode\n", ts->input_dev->name, ts->use_irq ? "interrupt" : "polling");
	return 0;

err_input_register_device_failed:
	input_free_device(ts->input_dev);

err_input_dev_alloc_failed:
err_alloc_data_failed:
err_check_functionality_failed:
	return ret;
}



static int synaptics_ts_remove(struct i2c_client *client)
{
	struct synaptics_ts_data *ts = i2c_get_clientdata(client);

#ifdef FEATURE_LGE_TOUCH_GRIP_SUPPRESSION	//20101216 seven.kim@lge.com [start]
	device_remove_file(&client->dev, &dev_attr_gripsuppression);
#endif //20101216 seven.kim@lge.com [end]

	unregister_early_suspend(&ts->early_suspend);
	if (ts->use_irq)
		free_irq(client->irq, ts);
	else
		hrtimer_cancel(&ts->timer);
	input_unregister_device(ts->input_dev);
	kfree(ts);
	return 0;
}

#if defined(CONFIG_PM)
static int synaptics_ts_suspend(struct device *device)
{
	pr_warning ("\n");

	return 0;
}

static int synaptics_ts_resume(struct device *device)
{
	pr_warning ("\n");

	return 0;
}
#endif

static void synaptics_ts_suspend_func(struct synaptics_ts_data *ts)
{
	int ret;

	init_stabled = 0;

	if (ts->use_irq)
		disable_irq_nosync(ts->client->irq);
	else
		hrtimer_cancel(&ts->timer);

	//ret = cancel_work_sync(&ts->init_delayed_work); //seven.kim@lge.com sunggyun.ryu recommand
	ret = cancel_delayed_work_sync(&ts->work);
	if (ret && ts->use_irq) /* if work was pending disable-count is now 2 */
		enable_irq(ts->client->irq);

#if 1 //20110222 seven.kim@lge.com blocked to prevent i2c error
	    ret = synaptics_ts_i2c_write_byte_data(ts->client, SYNAPTICS_CONTROL_REG, SYNAPTICS_CONTROL_SLEEP); /* sleep */
	    if (ret < 0)
		pr_err("synaptics_ts_suspend: synaptics_ts_i2c_write_byte_data failed\n");
#endif //end of seven

		if (ts->pdata->power_enable){

			 ret = ts->pdata->power_enable(0,true);
					if (ret < 0) {
						pr_debug("Touch power off failed\n");
						}

		}
}


static void synaptics_ts_resume_func(struct synaptics_ts_data *ts)
{
	int ret = 0;
	if (ts->pdata->power_enable){

		 ret = ts->pdata->power_enable(1,true);
		 		if (ret < 0) {
					pr_debug("Touch power on failed\n");
		 			}

	}

	init_stabled = 1;

	queue_delayed_work(synaptics_wq,
			&ts->work,msecs_to_jiffies(ts->pdata->ic_booting_delay));

   	if (ts->use_irq)
		enable_irq(ts->client->irq);

	if (!ts->use_irq)
		hrtimer_start(&ts->timer,
					ktime_set(0, ts->pdata->report_period+(ts->pdata->ic_booting_delay*1000000)),
					HRTIMER_MODE_REL);

// 20100826 jh.koo@lge.com, for stable initialization [START_LGE]
//	    schedule_delayed_work(&ts->init_delayed_work, msecs_to_jiffies(400));
// 20100826 jh.koo@lge.com, for stable initialization [END_LGE]
  	    memset(&ts_reg_data, 0x0, sizeof(ts_sensor_data));
	    memset(&prev_ts_data, 0x0, sizeof(ts_finger_data));
  	    memset(&curr_ts_data, 0x0, sizeof(ts_finger_data));

}

#ifdef CONFIG_HAS_EARLYSUSPEND
static void synaptics_ts_early_suspend(struct early_suspend *h)
{
	struct synaptics_ts_data *ts;
	ts = container_of(h, struct synaptics_ts_data, early_suspend);


//20110516 yongman.kwon@lge.com [LS855] Write description here in detail [START]

	{
		int i;
		for(i = 0; i < MAX_NUM_OF_FINGER; i++)
		{
			if(sent_event[i] == 1)
			{
				curr_ts_data.X_position[i] = (int)TS_SNTS_GET_X_POSITION(ts_reg_data.fingers_data[i][0], ts_reg_data.fingers_data[i][2]);
				curr_ts_data.Y_position[i] = (int)TS_SNTS_GET_Y_POSITION(ts_reg_data.fingers_data[i][1], ts_reg_data.fingers_data[i][2]);

				input_report_abs(ts->input_dev, ABS_MT_POSITION_X, curr_ts_data.X_position[i]);
				input_report_abs(ts->input_dev, ABS_MT_POSITION_Y, curr_ts_data.Y_position[i]);
				input_report_abs(ts->input_dev, ABS_MT_TOUCH_MAJOR, 0);
				input_report_abs(ts->input_dev, ABS_MT_WIDTH_MAJOR, 0);

				input_mt_sync(ts->input_dev);
				sent_event[i] = 0;
			}
		}
	}
//20110516 yongman.kwon@lge.com [LS855] Write description here in detail [END]

	synaptics_ts_suspend_func(ts);


}

static void synaptics_ts_late_resume(struct early_suspend *h)
{
	struct synaptics_ts_data *ts;
	ts = container_of(h, struct synaptics_ts_data, early_suspend);

	synaptics_ts_resume_func(ts);
}
#endif

static const struct i2c_device_id synaptics_ts_id[] = {
	{ "synaptics_ts", 0 },
	{ },
};

#if defined(CONFIG_PM)
static struct dev_pm_ops synaptics_ts_pm_ops = {
	.suspend 	= synaptics_ts_suspend,
	.resume 	= synaptics_ts_resume,
};
#endif

static struct i2c_driver synaptics_ts_driver = {
	.probe		= synaptics_ts_probe,
	.remove		= synaptics_ts_remove,
	.id_table	= synaptics_ts_id,
	.driver = {
		.name	= "synaptics_ts",
		.owner = THIS_MODULE,
	#if defined(CONFIG_PM)
		.pm		= &synaptics_ts_pm_ops,
	#endif
	},
};

static int __devinit synaptics_ts_init(void)
{
	int ret = 0;

	ret = i2c_add_driver(&synaptics_ts_driver);
	if (ret < 0) {
		pr_err("failed to i2c_add_driver\n");
		destroy_workqueue(synaptics_wq);
	}

	return ret;
}

static void __exit synaptics_ts_exit(void)
{
	i2c_del_driver(&synaptics_ts_driver);

	if (synaptics_wq)
		destroy_workqueue(synaptics_wq);
}

module_init(synaptics_ts_init);
module_exit(synaptics_ts_exit);

MODULE_DESCRIPTION("Synaptics Touchscreen Driver");
MODULE_LICENSE("GPL");

