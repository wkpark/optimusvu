

#ifndef __325_SYNAPTICS_TS_H
#define __325_SYNAPTICS_TS_H
#include <linux/earlysuspend.h>

//#define SYNAPTICS_FINGER_MAX					5  	// [20101111:geayoung.baek@lge.com] Max Touch 5EA

#define MAX_NUM_OF_FINGER			5

#define NUM_OF_EACH_FINGER_DATA_REG		5

struct synaptics_ts_platform_data {
	bool use_irq;
	unsigned long irqflags;
	unsigned short i2c_sda_gpio;
	unsigned short i2c_scl_gpio;
	unsigned short i2c_int_gpio;
	int (*power_enable)(int en, bool log_en);;
	unsigned short ic_booting_delay;		/* ms */
	unsigned long report_period;			/* ns */
	unsigned char num_of_finger;
	unsigned char num_of_button;
	int x_max;
	int y_max;
	unsigned char fw_ver;
	unsigned int palm_threshold;
	unsigned int delta_pos_threshold;
};

typedef struct {
	unsigned char m_QueryBase;
	unsigned char m_CommandBase;
	unsigned char m_ControlBase;
	unsigned char m_DataBase;
	unsigned char m_IntSourceCount;
	unsigned char m_FunctionExists;
} T_RMI4FuncDescriptor;

typedef struct
{
	unsigned char device_status_reg;						//0x13
	unsigned char interrupt_status_reg;					//0x14
	unsigned char finger_state_reg[3];					//0x15~0x17

	unsigned char fingers_data[MAX_NUM_OF_FINGER][NUM_OF_EACH_FINGER_DATA_REG];	//0x18 ~ 0x49

} ts_sensor_data;

typedef struct {
	unsigned char touch_status[MAX_NUM_OF_FINGER];
	unsigned int X_position[MAX_NUM_OF_FINGER];
	unsigned int Y_position[MAX_NUM_OF_FINGER];
	unsigned char width[MAX_NUM_OF_FINGER];
	unsigned char pressure[MAX_NUM_OF_FINGER];
} ts_finger_data;

struct synaptics_ts_data {
	uint16_t addr;
	struct i2c_client *client;
	struct input_dev *input_dev;
	struct synaptics_ts_platform_data *pdata;
	int use_irq;
	bool has_relative_report;
	struct hrtimer timer;
	struct delayed_work  work;
	uint16_t max[2];

	uint32_t flags;
	int reported_finger_count;
	int8_t sensitivity_adjust;
	int (*power_enable)(int en, bool log_en);;
// 20100504 jh.koo@lge.com, correction of finger space [START_LGE]
	unsigned int count;
	int x_lastpt;
	int y_lastpt;
// 20100504 jh.koo@lge.com, correction of finger space [END_LGE]
	struct early_suspend early_suspend;
// 20100826 jh.koo@lge.com, for stable initialization [START_LGE]
	struct delayed_work init_delayed_work;
// 20100826 jh.koo@lge.com, for stable initialization [END_LGE]
	unsigned char product_value; //product_value=0:misung panel  product_value=1 : LGIT panel
	char fw_rev;
	char manufcturer_id;
};


#endif
