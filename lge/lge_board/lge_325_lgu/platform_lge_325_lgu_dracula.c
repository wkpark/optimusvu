/* arch/arm/mach-msm/lge/board-i_vzw-bt.c
 * Copyright (C) 2009 LGE, Inc.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/types.h>
#include <linux/list.h>
#include <linux/err.h>
#include <linux/platform_device.h>
#include <asm/setup.h>
#include <mach/gpio.h>
#include <mach/vreg.h>
#include <mach/board_lge.h>
#include <linux/delay.h>
#include <linux/rfkill.h>
#include <linux/timer.h>
#include "gpiomux.h"
#include "gpio.h"


#define GPIO_NUM_TO_DETECT_LG_SPECIAL_MODE_BOOT		128
#define GPIO_NUM_TO_RESET_SPECIAL_MODE				70
#define BT_RESET_N 										138


//static unsigned dracula_mode_detect = 
//	GPIO_CFG(GPIO_NUM_TO_DETECT_LG_SPECIAL_MODE_BOOT, 0, GPIO_CFG_INPUT,  GPIO_CFG_NO_PULL, GPIO_CFG_2MA);

static unsigned dracula_special_mode_reset = 
	GPIO_CFG(GPIO_NUM_TO_RESET_SPECIAL_MODE, 0, GPIO_CFG_OUTPUT,  GPIO_CFG_PULL_DOWN, GPIO_CFG_2MA);


struct timer_list lgsm_reset_timer;

void timer_callback_set_special_mode(unsigned long arg)
{
	//if(arg == 0)
	gpio_direction_output(GPIO_NUM_TO_RESET_SPECIAL_MODE, 0);	// LOW   ... enable LDO
	printk("[%s] Reset BT End : VBAT On\n", __func__);
	del_timer(&lgsm_reset_timer);
}


 
int __init dracula_gpio_init(void)
{
	int ret = 0;
	//int gpio_stat = 0;
	//unsigned long gpioStat = 0;

	// INITIALIZE

#if 1
	// temporary merge to init GPIO_NUM_TO_RESET_SPECIAL_MODE 70
	ret = gpio_tlmm_config(dracula_special_mode_reset, GPIO_CFG_ENABLE);
	if (ret) 
	{
		printk(KERN_ERR "%s: gpio_tlmm_config(%#x)=fg%d\n",__func__, dracula_special_mode_reset, ret);
		return -EIO;
	}

	ret = gpio_request(GPIO_NUM_TO_RESET_SPECIAL_MODE, "dracula BT reset");
	if (ret) {
		pr_err("%s: gpio %d request failed\n", __func__, GPIO_NUM_TO_RESET_SPECIAL_MODE);
		return ret;
	}

	gpio_direction_output(GPIO_NUM_TO_RESET_SPECIAL_MODE, 0);	// LOW	

	printk("%s: gpio_tlmm_config(%#x)=fg%d\n",__func__, dracula_special_mode_reset, ret);


#else
	ret = gpio_tlmm_config(dracula_mode_detect, GPIO_CFG_ENABLE);
	if (ret) 
	{
		printk(KERN_ERR "%s: gpio_tlmm_config(%#x)=fg%d\n",__func__, dracula_mode_detect, ret);
		return -EIO;
	}

	ret = gpio_tlmm_config(dracula_special_mode_reset, GPIO_CFG_ENABLE);
	if (ret) 
	{
		printk(KERN_ERR "%s: gpio_tlmm_config(%#x)=fg%d\n",__func__, dracula_special_mode_reset, ret);
		return -EIO;
	}

	// CHECK BOOT MODE
	gpio_stat = gpio_get_value(GPIO_NUM_TO_DETECT_LG_SPECIAL_MODE_BOOT); 

	printk("[%s] gpio_get_value -> gpio_stat : %d\n", __func__, gpio_stat);

	if(gpio_stat == 0) // normal boot
	{
		// BT Reset Default is LOW
		gpio_direction_output(BT_RESET_N, 0);		

		// Reset BT Chip Using VBAT Off -> On
		printk("[%s] Reset BT Start : VBAT Off\n", __func__);
		gpio_direction_output(GPIO_NUM_TO_RESET_SPECIAL_MODE, 1);	// HIGH   ... disable LDO
		#if 1
		// use timer to decrease booting time
		init_timer(&lgsm_reset_timer);
		lgsm_reset_timer.function = timer_callback_set_special_mode;
		lgsm_reset_timer.data = (unsigned long)NULL;
		//lgsm_reset_timer.expires = jiffies + 1 * HZ;	// 1sec
		lgsm_reset_timer.expires = jiffies + (3*HZ)/10;	// 300 ms
		add_timer(&lgsm_reset_timer);		
		#else
		mdelay(100);
		gpio_direction_output(GPIO_NUM_TO_RESET_SPECIAL_MODE, 0);	// LOW   ... enable LDO
		printk("[%s] Reset BT End : VBAT On\n", __func__);
		#endif

	}
	else		// lg special mode boot 
	{
		printk("[%s] lg special mode boot : set GPIO_NUM_TO_RESET_SPECIAL_MODE LOW\n", __func__);
		//gpio_direction_output(BT_RESET_N, 0);							// LOW
		gpio_direction_output(GPIO_NUM_TO_RESET_SPECIAL_MODE, 0);	// LOW
	}
#endif

	return 0;
}

