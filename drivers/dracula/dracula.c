/*
 * dracula
 *
 * Copyright (C) 2009 LGE, Inc.
 *
 * Author: giwon kang
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

/**
	@brief		 dracula
 
	@author		 giwon kang
	@date		 17-AUG-2011
 
	@version	 V1.00		 17-AUG-2011		 giwon kang	 Create
*/


#include <linux/platform_device.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <linux/timer.h>
#include <linux/err.h>

#include <linux/input.h>
#include <linux/wakelock.h>  

#include <mach/gpio.h>
#include <mach/board_lge.h>

#include "../../arch/arm/mach-msm/gpiomux.h"
#include "../../arch/arm/mach-msm/gpio.h"

#define GPIO_NUM_TO_DETECT_LG_SPECIAL_MODE_BOOT	141
#define BT_WAKE 137


static unsigned dracula_disable_bt_wake = 
	GPIO_CFG(BT_WAKE, 0, GPIO_CFG_INPUT, GPIO_CFG_PULL_DOWN, GPIO_CFG_2MA);

// read
static ssize_t dracula_lg_smb_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	int ret = 0;
	int gpio_stat = 0;
	char data = 0;

	gpio_stat = gpio_get_value(GPIO_NUM_TO_DETECT_LG_SPECIAL_MODE_BOOT); 

	printk("[%s] gpio_get_value -> gpio_stat : %d\n", __func__, gpio_stat);


	if(gpio_stat == 0)
		data = 0;		// normal boot
	else
		data = 1;		// lg special mode boot

	ret = sprintf(buf, "%d\n", data);

	return (ssize_t)ret;
}

// write
static ssize_t dracula_lg_smb_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t size)
{
	int ret = 0;
	// write GPIO, but it is not necessary.
    //return (ssize_t)(strlen(buf) + 1);
	return (ssize_t)ret;
}


// read
static ssize_t dracula_bt_active_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	int ret = 0;

	/*
	int gpio_stat = 0;
	char data = 0;

	gpio_stat = gpio_get_value(GPIO_NUM_TO_DETECT_LG_SPECIAL_MODE_BOOT); 

	printk("[%s] gpio_get_value -> gpio_stat : %d\n", __func__, gpio_stat);


	if(gpio_stat == 0)
		data = 0;		// normal boot
	else
		data = 1;		// lg special mode boot

	ret = sprintf(buf, "%d\n", data);
	*/

	return (ssize_t)ret;
}

// write
static ssize_t dracula_bt_active_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t size)
{
	int ret = 0;
#if 1
	// configure_uart_gpios : Disable
	int i = 0;
	int ret_gpio_config = 0;
	int uart_gpios[] = {53, 54, 55, 56};

	if( buf[0] == '1' )
	{
		printk("[%s] buf[0] : %d\n", __func__, buf[0]);
	}
	else	//if( buf[0] == '0' )
	{
		gpio_direction_output(BT_WAKE, 0);	// disable BT_WAKE
		gpio_tlmm_config(dracula_disable_bt_wake, GPIO_CFG_ENABLE);

		for (i = 0; i < ARRAY_SIZE(uart_gpios); i++) 
		{
			ret_gpio_config = msm_gpiomux_put(uart_gpios[i]);
			//if (unlikely(ret_gpio_config))
			//	return ret_gpio_config;
		}
		printk("[%s] %d\n", __func__, buf[0]);
	}
#else
	if( buf[0] == '1' )
	{
		gpio_set_value(BT_WAKE, 1);	// set BT_ACTIVE High
		printk("[%s] gpio_set_value(BT_WAKE, 1) -> ret : %d\n", __func__, ret);
	}
	else
	{
		gpio_set_value(BT_WAKE, 0);	// set BT_ACTIVE Low
		printk("[%s] gpio_set_value(BT_WAKE, 0) -> ret : %d\n", __func__, ret);
	}
#endif

	return (ssize_t)ret;
}

/* [START] 2011-12-09, [giwon.kang@lge.com], [Dracula] change permission 0666 -> 0644 */
// device for "lg special mode boot"
static DEVICE_ATTR(lg_smb, 0644, dracula_lg_smb_show, dracula_lg_smb_store);

// device for BT_ACTIVE
static DEVICE_ATTR(bt_active, 0644, dracula_bt_active_show, dracula_bt_active_store);
/* [END] 2011-12-09, [giwon.kang@lge.com], [Dracula] change permission 0666 -> 0644 */

#if 0
static struct attribute *dracula_lg_smb_attributes[] = {
    &dev_attr_lg_smb.attr,
    NULL
};


static const struct attribute_group dracula_lg_smb_group = {
    .attrs = dracula_lg_smb_attributes,
};
#endif

static int __devinit dracula_probe(struct platform_device *pdev)
{
    int ret = 0;

	printk("%s\n", __func__);

#if 1
	ret = device_create_file(&pdev->dev, &dev_attr_lg_smb);
	if (ret) 
	{
		printk("[%s] device_create_file -> res : %d\n", __func__, ret);
		goto err_lg_smb_create_fail;

	}

	ret = device_create_file(&pdev->dev, &dev_attr_bt_active);
	if (ret) 
	{
		printk("[%s] device_create_file -> res : %d\n", __func__, ret);
		goto err_bt_active_create_fail;

	}


	return 0;

	err_bt_active_create_fail:	 
		device_remove_file(&pdev->dev, &dev_attr_bt_active);

	err_lg_smb_create_fail:	 
		device_remove_file(&pdev->dev, &dev_attr_lg_smb);

	return -1;

#else
	ret = sysfs_create_group(&pdev->dev.kobj, &dracula_lg_smb_group);
	if (ret) {
		printk(KERN_ERR "[dracula] sysfs_create_group ERROR\n");
		goto err_lg_smb_sysfs_fail;
	}

	printk(KERN_INFO "[dracula] sysfs_create_group OK\n");

    return 0;

	err_lg_smb_sysfs_fail:	 
		sysfs_remove_group(&pdev->dev.kobj, &dracula_lg_smb_group);

	return -ENOSYS;
#endif
}

static int dracula_remove(struct platform_device *pdev)
{
	int ret = 0;

#if 1
	device_remove_file(&pdev->dev, &dev_attr_lg_smb);
#else
	sysfs_remove_group(&pdev->dev.kobj, &dracula_lg_smb_group);
#endif

	return ret;	
}


int dracula_suspend(struct platform_device *dev, pm_message_t state)
{

	return 0;
}

int dracula_resume(struct platform_device *dev)
{

	return 0;
}

static struct platform_device dracula_device = {
	.name = "dracula",
	.id		= -1,
};

static struct platform_driver dracula_driver = {
    .probe      = dracula_probe,
    .remove     = dracula_remove,

    .suspend    = dracula_suspend,
    .resume     = dracula_resume,


    .driver = {
        .name   = "dracula",
        .owner  = THIS_MODULE,
    },
};

static int __init dracula_init(void)
{
	printk("[%s]\n", __func__);

	platform_device_register(&dracula_device);

    return platform_driver_register(&dracula_driver);
}

static void __exit dracula_exit(void)
{
    platform_driver_unregister(&dracula_driver);
	platform_device_unregister(&dracula_device);
}

module_init(dracula_init);
module_exit(dracula_exit);
MODULE_AUTHOR("giwon.kang@lge.com");
MODULE_DESCRIPTION("dracula");
MODULE_LICENSE("GPL");

