#define pr_fmt(fmt)		"sysprofd: " fmt

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/syscalls.h>
#include <linux/fcntl.h>
#include <linux/socket.h>
#include <linux/inet.h>
#include <linux/in.h>
#include <linux/un.h>
#include <linux/net.h>
#include <linux/kthread.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/cpu.h>

#include <linux/sched_prof.h>

#define KSYSPROF_DAEMON	"ksysprofd"
#define KSYSPROF_DEFAULT_TIMEOUT	(HZ / 2)

extern int task_trace_flag;
extern int stop_wait;
extern void trace_done_wakeup(int);
DEFINE_PER_CPU(unsigned long, ksend_count);

static DEFINE_PER_CPU(int, trace_fd);
static DEFINE_PER_CPU(struct task_struct *, sysprofd);

#if defined (USE_NETWORK) || defined (USE_DSOCKET)
static DEFINE_PER_CPU(struct socket *, socket);
#ifdef CONFIG_LG_SYSPROF_KEVENT
static DEFINE_PER_CPU(struct socket *, kevt_socket);
#endif
#endif

#ifdef USE_NETWORK
char sysprof_server[IPADDR_LEN];
#endif

#ifdef CONFIG_LG_SYSPROF_KEVENT
DECLARE_PER_CPU(kevent_info_t, kevent_trace);
static DEFINE_PER_CPU(int, kevt_fd);
#endif

/* Local Function */
static __attribute__((always_inline)) int get_current_event_idx(int, int *, int *);
static __attribute__((always_inline)) int get_current_trace_idx(int, int *, int *);
void clean_kevent_data_container(int cpu, int *fd);
void clean_prof_data_container(int cpu);

DEFINE_PER_CPU(unsigned long, ksend_count);
static DEFINE_PER_CPU(int, trace_fd);
static DEFINE_PER_CPU(struct task_struct *, sysprofd);
char sysprof_server[IPADDR_LEN];
int sysprof_hwcache_pid = -1;

/* Extern Function */

#define _kopen(filename, flags, mode)	sys_open(filename, flags, mode)
//commented by hoyoung2.jung
/*
static __attribute__((always_inline)) int kopen(char *filename, int flags, int mode)
{
	int fd;
	mm_segment_t old_fs = get_fs();

	set_fs(KERNEL_DS);
	fd = _kopen(filename, flags, mode);
	set_fs(old_fs);
	return fd;
}
*/
//end of hoyoung2.jung


#define kclose(fd) { \
	mm_segment_t old_fs = get_fs(); \
	set_fs(KERNEL_DS); \
	sys_close(fd) \
	set_fs(old_fs); \
}

static int _kwrite(int fd, char *buf, size_t size)
{
	size_t remain = size;
	int wcnt = 0, total_wcnt = 0;
	
	do {
		wcnt = sys_write(fd, buf, remain);
		if (wcnt < 0) {
			pr_err("kernel file write error(%d) wsize %d remain %d\n", wcnt, size, remain);
			/* TODO: handle error code */
			break;
		}
		remain -= wcnt;
		total_wcnt += wcnt;
	} while (remain > 0);

	return total_wcnt ? total_wcnt : wcnt;
}

static __attribute__((always_inline)) int kwrite(int fd, char *buf, size_t size)
{
	int wcnt = 0;
	mm_segment_t old_fs = get_fs();

	set_fs(KERNEL_DS);
	wcnt = _kwrite(fd, buf, size);
	set_fs(old_fs);

	return wcnt;
}

static __attribute__((always_inline)) int write_buffer(int fd, char *buf, int idx, int sizeof_trow)
{
	int write_idx, ret = 0;
	size_t wsize;

	if (idx >= MAX_SEND_IDX) {
		write_idx = MAX_SEND_IDX;
	} else {
		write_idx = idx;
	}
	wsize = write_idx * sizeof_trow;
	ret = kwrite(fd, buf, wsize);
	if (ret < 0)
		pr_err("kernel file write error(fd:%d ret:%d)\n", fd, ret);

	return write_idx;
}

typedef struct wbuf_info wbuf_info_t;
struct wbuf_info {
	char *start;	/* start point of write buffer */
	int sizeof_row;	/* size of a member of write buffer */
	int max_idx;
	atomic_t *read_idx;	/* the address of atomic variable to set read_index */
	int send_count;	/* how many times are packets sended */
};


static void write_log(int cpu, int fd, int cur_rp, int cur_wp, wbuf_info_t *winfo)
{
	int wrap_around = FALSE;
	int sizeof_trow = winfo->sizeof_row;
	int max_idx = winfo->max_idx;
	int write_idx, remain_idx;
	int dist_to_end = 0;	/* distance to end idx */

	char *rp, *start;
	char *buf;

	/* get start dirty buffer address */
	start = winfo->start;
	rp = (char *)(start + (cur_rp * sizeof_trow));

	/* get dirty buffer area to be written */
	remain_idx = cur_wp - cur_rp;
	if (remain_idx >= 0)
		dist_to_end = 0;
	else {
		remain_idx += max_idx;
		dist_to_end = max_idx - cur_rp;
	}

	/* write log from current read_idx to end of buffer */
	while (dist_to_end > 0) {
		wrap_around = TRUE;
		buf = rp;

		write_idx = write_buffer(fd, buf, dist_to_end, sizeof_trow);

		remain_idx -= write_idx;
		dist_to_end -= write_idx;

		cur_rp += write_idx;
		if (cur_rp == max_idx)
			cur_rp = 0;

		per_cpu(ksend_count, cpu)++;
		atomic_set(winfo->read_idx, cur_rp);
		rp = (char *)(start + (cur_rp * sizeof_trow));
	}

	if (dist_to_end != 0) {
		/* TODO: error */
	}

	if (wrap_around == TRUE)
		rp = start;

	/* write log from 0 idx to current write_idx */
	while (remain_idx > 0) {
		buf = rp;

		write_idx = write_buffer(fd, buf, remain_idx, sizeof_trow);

		remain_idx -= write_idx;
		cur_rp += write_idx;

		per_cpu(ksend_count, cpu)++;
		atomic_set(winfo->read_idx, cur_rp);
		rp = (char *)(start + (cur_rp * sizeof_trow));
	}
}

static __attribute__((always_inline)) void write_task_log(int cpu, int fd, int cur_rp, int cur_wp)
{
	wbuf_info_t winfo;
	trace_info_t *ti = &per_cpu(task_trace, cpu);

	winfo.sizeof_row = sizeof(trow_t);
	winfo.max_idx = MAX_TRACE_IDX;
	winfo.start = (char *)&ti->trow[0];
	winfo.read_idx = &ti->read_index;
	if (cur_wp < cur_rp)
		ti->read_idx_flag ^= 0x01;
	write_log(cpu, fd, cur_rp, cur_wp, &winfo);
}

static __attribute__((always_inline)) void write_event_log(int cpu, int fd, int cur_rp, int cur_wp)
{
	wbuf_info_t winfo;
	kevent_info_t *keinfo = &per_cpu(kevent_trace, cpu);

	winfo.sizeof_row = sizeof(kevent_t);
	winfo.max_idx = MAX_KEVENT_IDX;
	winfo.start = (char *)&keinfo->log[0];
	winfo.read_idx = &keinfo->read_idx;
	write_log(cpu, fd, cur_rp, cur_wp, &winfo);
}


static void check_event_state(int cpu, int show_flag)
{
	int cur_rp, cur_wp;

	/* wait until profile flag is on or cpu is on */
	if (get_current_event_idx(cpu, &cur_rp, &cur_wp)) {
		if ((task_trace_flag == 0) || (cpu_is_offline(cpu))) {
//			#ifdef CONFIG_SYSPROF_DEBUG
			if (task_trace_flag == 0)
				pr_info("EVENT DATA: goto sleep due to cpu[%d] SEND all data cur_rp %d, cur_wp %d\n", cpu, cur_rp, cur_wp);
			if (cpu_is_offline(cpu))
				pr_info("EVENT DATA: goto sleep due to cpu[%d] turned off\n", cpu);
//			#endif
		}
	}
	pr_debug("EVENT DATA : cpu %d cur_rp %d cur_wp %d %s\n", cpu, cur_rp, cur_wp, current->comm);
	if (likely(cur_rp != cur_wp)) {
		int gap = cur_wp - cur_rp;
		if (gap < 0)
			gap += MAX_KEVENT_IDX;

		if (gap >= (MAX_KEVENT_IDX / 4) || task_trace_flag == 0)
			write_event_log(cpu, per_cpu(kevt_fd, cpu), cur_rp, cur_wp);
		if (show_flag == 1) {
			pr_info("EVENT DATA: writing cpu[%d] cur_rp %d cur_wp %d\n", cpu, cur_rp, cur_wp);
		}
	}
}

static void check_trace_state(int cpu, int show_flag, long *timeout)
{
	int cur_rp, cur_wp;

	/* wait until profile flag is on or cpu is on */
	if (get_current_trace_idx(cpu, &cur_rp, &cur_wp)) {
		if ((task_trace_flag == 0) || (cpu_is_offline(cpu))) {
			*timeout = MAX_SCHEDULE_TIMEOUT;
			#ifdef CONFIG_SYSPROF_DEBUG
			if (task_trace_flag == 0)
				pr_info("TRACE DATA: goto sleep due to cpu[%d] SEND all data cur_rp %d, cur_wp %d\n", cpu, cur_rp, cur_wp);
			if (cpu_is_offline(cpu))
				pr_info("TRACE DATA: goto sleep due to cpu[%d] turned off\n", current->comm, cpu);
			#endif

#ifdef CONFIG_LG_SYSPROF_KEVENT
			if (per_cpu(kevt_fd, cpu) >= 0) {
				clean_kevent_data_container(cpu, &per_cpu(kevt_fd, cpu));
			}
#endif
			clean_prof_data_container(cpu);

			/* if profiler receive stop signal, 
			 * it will wake up process for stopping process */
			pr_info("cpu %d stop_wait %d\n", cpu, stop_wait);
			if (stop_wait == 1) {
				trace_done_wakeup(cpu);
			}
		}
	}
//	pr_debug("TRACE DATA : cpu %d cur_rp %d cur_wp %d timemout %ld %s %d\n", cpu, cur_rp, cur_wp, *timeout, current->comm, __LINE__);
	if (likely(cur_rp != cur_wp)) {
		if (show_flag == 1) {
			pr_info("TRACE DATA: writing cpu[%d] cur_rp %d cur_wp %d\n", cpu, cur_rp, cur_wp);
		}
		write_task_log(cpu, per_cpu(trace_fd, cpu), cur_rp, cur_wp);
	}
	get_current_trace_idx(cpu, &cur_rp, &cur_wp);
	pr_info("TRACE_DATA : after write cur rp %d cur_wp %d\n", cur_rp, cur_wp);
}

/* get current read / write index of trace data
 * return value
 * if read / write index is same, then return TRUE. */
static __attribute__((always_inline)) int get_current_trace_idx(int cpu, int *cur_rp, int *cur_wp)
{
	trace_info_t *ti = &per_cpu(task_trace, cpu);

	*cur_rp = atomic_read(&ti->read_index);
	*cur_wp = atomic_read(&ti->index);
	return (*cur_rp == *cur_wp);
}

static __attribute__((always_inline)) int get_current_event_idx(int cpu, int *cur_rp, int *cur_wp)
{
	kevent_info_t *ei = &per_cpu(kevent_trace, cpu);

	*cur_rp = atomic_read(&ei->read_idx);
	*cur_wp = atomic_read(&ei->write_idx);
	return (*cur_rp == *cur_wp);
}

typedef struct kern_sock kern_sock_t;
struct kern_sock {
	int family;
	int type;
	int protocol;
	union {
		struct sockaddr_in in_addr;
		struct sockaddr_un un_addr;
	} d;
};

int socket_conn(kern_sock_t *ksocket, struct socket **sock, int *fd)
{
	int ret = 0;
	int family = ksocket->family;
	int type = ksocket->type;
	int protocol = ksocket->protocol;
	int addr_size = 0;
	struct sockaddr *dest_addr;

	if (family == AF_UNIX) {
		dest_addr = (struct sockaddr *)&ksocket->d.un_addr;
		addr_size = sizeof(typeof(ksocket->d.un_addr));
	} else {
		dest_addr = (struct sockaddr *)&ksocket->d.in_addr;
		addr_size = sizeof(typeof(ksocket->d.in_addr));
	}

	ret = sock_create_kern(family, type, protocol, sock);
	if (ret < 0) {
		pr_err("Error(%d) during creation of socket; terminating \n", ret);
		return ret;
	}
	ret = kernel_connect(*sock, dest_addr, addr_size, O_RDWR);
	if (ret < 0) {
		pr_err("Error(%d) during socket connection; terminating \n", ret);
		return ret;
	}

	*fd = sock_map_fd(*sock, type & (O_CLOEXEC | O_NONBLOCK));
	if (*fd < 0) {
		pr_err("Error during sock_map_fd; terminating \n");
		sock_release(*sock);
		return -ENOENT;
	}
	return 0;
}

/* use file or socket to carry profile data */
static int prepare_profile_data_container(int cpu)
{
	int ret = 0;
	kern_sock_t ksocket;

#if defined (USE_NETWORK)
	ksocket.family = AF_INET;
	#ifdef USE_UDP
	ksocket.type = SOCK_DGRAM;
	ksocket.protocol = IPPROTO_UDP;
	#else
	ksocket.type = SOCK_STREAM;
	ksocket.protocol = IPPROTO_TCP;
	#endif

	memset(&ksocket.d.in_addr, 0, sizeof(ksocket.d.in_addr));
	ksocket.d.in_addr.sin_family = AF_INET;
	ksocket.d.in_addr.s_addr = in_aton(sysprof_server);
	ksocket.d.in_addr.sin_port = htons(SYSPROF_CPU_PORT(cpu));

	ret = socket_conn(&ksocket, &per_cpu(socket, cpu), fd);
	if (ret < 0)
		return ret;

	pr_info("%s %s socket for data is created \n", current->comm,
			(ksocket.family == AF_UNIX)?"Domain":"Inet");

#elif defined (USE_DSOCKET)
	int magic_key = 0xBADFEE00 + cpu;

	ksocket.family = AF_UNIX;
	ksocket.type = SOCK_STREAM;
	ksocket.protocol = IPPROTO_IP;
	memset(&ksocket.d.un_addr, 0, sizeof(ksocket.d.un_addr));
	ksocket.d.un_addr.sun_family = AF_UNIX;
	sprintf(ksocket.d.un_addr.sun_path, "%s", SYSPROF_UDS_PATH);

	ret = socket_conn(&ksocket, &per_cpu(socket, cpu), &per_cpu(trace_fd, cpu));
	if (ret < 0)
		return ret;

	pr_info("%s %s socket for data is created \n", current->comm,
			(ksocket.family == AF_UNIX)?"Domain":"Inet");

	/* send magic number to announce that it is socket for kevent */
	ret = kwrite(per_cpu(trace_fd, cpu), (char *)&magic_key, sizeof(int));
	if (ret < 0) {
		pr_err("cpu[%d], data magic_key send error(%d)\n", cpu, ret);
		/* TODO: handle error code */
	}

#elif defined (USE_FILESYSTEM)
	char filename[256];

	sprintf(filename, "%s%d", TRACE_FILENAME, cpu);
	per_cpu(trace_fd, cpu) = kopen(filename, O_CLOEXEC | O_RDWR | O_CREAT | O_LARGEFILE | O_TRUNC, 0755);
	if (per_cpu(trace_fd, cpu) < 0) {
		pr_err("Error during file open \n");
		return -EFAULT;
	}
	pr_info("File(%s:%d) is opened\n", filename, per_cpu(trace_fd, cpu));

#endif
	return 0;
}

int prepare_profile_kevent_container(int cpu, int *fd)
{
	kern_sock_t ksocket;
	int ret = 0;

#if defined (USE_NETWORK)
	ksocket.family = AF_INET;
	#ifdef USE_UDP
	ksocket.type = SOCK_DGRAM;
	ksocket.protocol = IPPROTO_UDP;
	#else
	ksocket.type = SOCK_STREAM;
	ksocket.protocol = IPPROTO_TCP;
	#endif
	memset(&ksocket.d.in_addr, 0, sizeof(ksocket.d.in_addr));
	ksocket.d.in_addr.sin_family = AF_INET;
	ksocket.d.in_addr.s_addr = in_aton(sysprof_server);
	ksocket.d.in_addr.sin_port = htons(SYSPROF_KEVENT_PORT(cpu));

	socket_conn(&ksocket, &per_cpu(kevt_socket, cpu), fd);

#elif defined (USE_DSOCKET)
	int magic_key = 0xBADFEE00 + cpu + 0x10;

	ksocket.family = AF_UNIX;
	ksocket.type = SOCK_STREAM;
	ksocket.protocol = IPPROTO_IP;
	memset(&ksocket.d.un_addr, 0, sizeof(ksocket.d.un_addr));
	ksocket.d.un_addr.sun_family = AF_UNIX;
	sprintf(ksocket.d.un_addr.sun_path, "%s", SYSPROF_UDS_PATH);

	ret = socket_conn(&ksocket, &per_cpu(kevt_socket, cpu), fd);
	if (ret < 0) {
		return -EFAULT;
	}
	pr_info("%s %s socket for kevent is created \n", current->comm,
			(ksocket.family == AF_UNIX)?"Domain":"Inet");

	/* send magic number to announce that it is socket for kevent */
	ret = kwrite(*fd, (char *)&magic_key, sizeof(int));
	if (ret < 0) {
		pr_err("cpu[%d] kevent magic_key send error(%d)\n", cpu, ret);
		/* TODO: handle error code */
	}

#elif defined (USE_FILESYSTEM)
	char kevt_filename[256];

	if (cpu != 0) {
		return -EFAULT;
	}
	sprintf(kevt_filename, "%s", KEVENT_FILENAME);
	*fd = kopen(kevt_filename, O_APPEND | O_CLOEXEC | O_RDWR | O_CREAT | O_TRUNC, 0755);
	if (*fd < 0) {
		pr_err("Error during file open \n");
		return -EFAULT;
	}
	pr_info("Kevent Log File(%s:%d) is opened\n", kevt_filename, *fd);
#endif
	return 0;
}

void clean_prof_data_container(int cpu)
{
#if defined (USE_NETWORK) || defined (USE_DSOCKET)
	struct socket **sk = &per_cpu(socket, cpu);
	if (*sk)
		sock_release(*sk);
	*sk = NULL;
	pr_info("%s cpu %d profile data socket released\n", current->comm, cpu);
#elif defined (USE_FILESYSTEM)
	int *fd = &per_cpu(trace_fd, cpu);
	if (*fd >= 0)
		kclose(*fd);
	*fd = -1;
	pr_info("%s cpu %d profile data file closed\n", current->comm, cpu);
#endif
}

void clean_kevent_data_container(int cpu, int *fd)
{
#if defined (USE_NETWORK) || defined (USE_DSOCKET)
	struct socket **sk = &per_cpu(kevt_socket, cpu);
	if (*sk)
		sock_release(*sk);
	*sk = NULL;
	pr_info("%s cpu %d kevent socket released\n", current->comm, cpu);
#elif defined (USE_FILESYSTEM)
	if (*fd >= 0)
		kclose(*fd);
	*fd = -1;
	pr_info("%s cpu %d kevent file closed\n", current->comm, cpu);
#endif
}

static int prepare_container(int cpu)
{
	int ret = 0;

	ret = prepare_profile_data_container(cpu);
	if (ret < 0) {
		pr_err("Can't establish data container\n");
		return -EFAULT;
	}
	#ifdef CONFIG_LG_SYSPROF_KEVENT	
	ret = prepare_profile_kevent_container(cpu, &per_cpu(kevt_fd, cpu));
	if (ret < 0) {
		pr_err("Can't establish kevent container\n");
		return -EFAULT;
	}
	#endif
	return ret;

}
static int ksysprofd(void *data)
{
	int cpu = (int)data;
	int ret;
	signed long timeout = KSYSPROF_DEFAULT_TIMEOUT;
	struct task_struct *tsk = current;
	int show_flag = 0;

	ignore_signals(tsk);

//	pr_info("%s %d is started\n", current->comm, __LINE__);

	/* wait for profile start */
	pr_info("%s/%d is called and sleep now\n", __func__, cpu);
	set_current_state(TASK_INTERRUPTIBLE);
	schedule();
	set_current_state(TASK_RUNNING);
	pr_info("%s/%d is waked up\n", __func__, cpu);

	ret = prepare_container(cpu);
	if (ret < 0)
		return -EFAULT;

	/* 'timeout' variable can only have either
	 * KSYSPROF_DEFAULT_TIMEOUT or MAX_SCHEDULE_TIMEOUT
	 *
	 * when CPU went off or trace flag went off
	 * (it means profiling was turned off), 'timeout' has
	 * MAX_SCHEDULE_TIMEOUT. */
	for (;;) {
		set_current_state(TASK_INTERRUPTIBLE);
		schedule_timeout(timeout);
		set_current_state(TASK_RUNNING);

		if (timeout == MAX_SCHEDULE_TIMEOUT) {
			pr_info("%s %d : cpu[%d:%p] wakeup to restart task tracing\n", __func__, __LINE__, cpu, current);
			ret = prepare_container(cpu);
			if (ret < 0)
				return -EFAULT;
			show_flag = 1;
		} else {
			show_flag = 0;
		}

		timeout = KSYSPROF_DEFAULT_TIMEOUT;
#ifdef CONFIG_LG_SYSPROF_KEVENT
		/* must be checked before checking trace state */
		check_event_state(cpu, show_flag);
#endif
		check_trace_state(cpu, show_flag, &timeout);

		if (kthread_should_stop()) {
			break;
		}
//		pr_info("ksysprofd 1pass loop done!!! timeout %lu\n", timeout);

#if 0	/* Temp : jay.sim */
	   {
		   static int loop_cnt = 0;
		   extern void v7_flush_kern_cache_all(void);
		   extern void v7_flush_user_cache_all(void);

		   loop_cnt++;
		   if ((loop_cnt % 100) == 0) {
		   s64 ecnt1 = 0;
		   s64 ecnt2 = 0;
		   s64 ecnt3 = 0;
		   pr_info("========== CACHE Invalidate ==========================\n");
		   invalidate I & D cache
		   asm volatile("mcr   p15, 0, %0, c7, c7, 0" : : "r"(value));
		   clean & invalidate D cache
		   asm volatile("mcr   p15, 0, %0, c7, c14, 0" : : "r"(value));
		   dsb();
		   pr_info("before flushing cache\n");
		   v7_flush_kern_cache_all();
		   v7_flush_user_cache_all();
		   pr_info("after flushing cache\n");
		   pr_info("======================================================\n");
		   loop_cnt = 0;
		   }
	   }
#endif
	}
	return 0;
}


static int __cpuinit sysprof_callback(struct notifier_block *nb, unsigned long action, void *cpu)
{
	int hotcpu = (unsigned long)cpu;
	struct task_struct *p;

	switch (action) {
		case CPU_UP_PREPARE:
		case CPU_UP_PREPARE_FROZEN:
			if (per_cpu(sysprofd, hotcpu) != NULL) {
				pr_info("CPU_UP but "KSYSPROF_DAEMON" %d is already created\n", hotcpu);
				return NOTIFY_OK;
			}

			p = kthread_create(ksysprofd, cpu, KSYSPROF_DAEMON"/%d", hotcpu);
			if (IS_ERR(p)) {
				pr_err(KSYSPROF_DAEMON" for %i failed\n", hotcpu);
				return notifier_from_errno(PTR_ERR(p));
			}
			kthread_bind(p, hotcpu);
			per_cpu(sysprofd, hotcpu) = p;
			pr_info(KSYSPROF_DAEMON"/%d : kthread_create is called\n", hotcpu);
			break;

		case CPU_ONLINE:
		case CPU_ONLINE_FROZEN:
			wake_up_process(per_cpu(sysprofd, hotcpu));
			pr_info(KSYSPROF_DAEMON"/%d : wake_up_process is called\n", hotcpu);
			break;
#ifdef CONFIG_HOTPLUG_CPU
		case CPU_UP_CANCELED:
		case CPU_UP_CANCELED_FROZEN:
			if (!per_cpu(sysprofd, hotcpu))
				break;
			break;

		case CPU_DEAD:
		case CPU_DEAD_FROZEN:
			pr_info(KSYSPROF_DAEMON"/%d : cpu is dead\n", hotcpu);
			break;
#endif
	}
	return NOTIFY_OK;
}

static struct notifier_block __cpuinitdata sysprof_cpu_nb = {
	.notifier_call = sysprof_callback,
};

static int __init ksysprof_daemon_init (void)
{
	void *cpu = (void *)(long)smp_processor_id();
	int err, i;

	for (i = 0; i < NR_CPUS; i++) {
		per_cpu(trace_fd, i) = -1;
		per_cpu(kevt_fd, i) = -1;
	}

	err = sysprof_callback(&sysprof_cpu_nb, CPU_UP_PREPARE, cpu);
	BUG_ON(err == NOTIFY_BAD);
	sysprof_callback(&sysprof_cpu_nb, CPU_ONLINE, cpu);
	register_cpu_notifier(&sysprof_cpu_nb);
	return 0;
}

void wakeup_sysprofd(int cpu)
{
	struct task_struct *p = per_cpu(sysprofd, cpu);
	if (p) {
		wake_up_process(p);
		pr_info("wakeup process "KSYSPROF_DAEMON"/%d:%p\n", cpu, p);
	}
}
EXPORT_SYMBOL(wakeup_sysprofd);


static void __exit ksysprof_daemon_exit (void)
{
	int i;
	struct task_struct *p;

	for (i = 0; i < NR_CPUS; i++) {
		p = per_cpu(sysprofd, i);
		if (p) {
			kthread_stop(p);
		}
	}
}

MODULE_LICENSE("GPL");
early_initcall(ksysprof_daemon_init);
