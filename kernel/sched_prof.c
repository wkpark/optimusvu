#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/seq_file.h>
#include <linux/kallsyms.h>
#include <linux/utsname.h>

#include <linux/sched_prof.h>
#include <linux/socket.h>
#include <linux/inet.h>
#include <linux/in.h>
#include <linux/net.h>
#include <linux/perf_event.h>
#include <asm/percpu.h>

extern void wakeup_sysprofd(int cpu);

#ifdef CONFIG_LG_SYSPROF_HWCACHE
extern atomic_t event_cnt;
atomic_t evt_attr_cnt;
atomic_t perf_event_cnt;
#endif

int stop_wait = 0;
int alloc_cnt;
static DECLARE_WAIT_QUEUE_HEAD(trace_stop_wait);

/*
 * This allows printing both to /proc/sched_debug and
 * to the console
 */
#define SEQ_printf(m, x...)			\
 do {						\
	if (m)					\
		seq_printf(m, x);		\
	else					\
		printk(x);			\
 } while (0)
#ifndef CONFIG_SCHED_DEBUG
#define SPLIT_NS(x) nsec_high(x), nsec_low(x)
/*
 * Ease the printing of nsec fields:
 */
//commented by hoyoung2.jung
/*
static long long nsec_high(unsigned long long nsec)
{
	if ((long long)nsec < 0) {
		nsec = -nsec;
		do_div(nsec, 1000000);
		return -nsec;
	}
	do_div(nsec, 1000000);

	return nsec;
}

static unsigned long nsec_low(unsigned long long nsec)
{
	if ((long long)nsec < 0)
		nsec = -nsec;

	return do_div(nsec, 1000000);
}
*/
//end of hoyoung 
#endif

int trace_has_started = 0;
int task_trace_flag = 0;
EXPORT_SYMBOL(task_trace_flag);

DEFINE_PER_CPU(trace_info_t, task_trace);
DEFINE_PER_CPU(unsigned int, gseq_no);
DEFINE_PER_CPU(extra_info_t, int_data);

int prepare_task_tracing(void);
void stop_task_tracing(void);

#ifdef CONFIG_LG_SYSPROF_KEVENT
DEFINE_PER_CPU(kevent_info_t, kevent_trace);
int prepare_kevent_tracing(void);
static void __sysprof_kevent_log(char *, char *, char *, unsigned int);
#endif

int prepare_task_tracing(void)
{
	int i;
	trace_info_t *ti;

	for (i = 0; i < NR_CPUS; i++) {
		ti = &per_cpu(task_trace, i);
		if (ti->trow == NULL) {
			pr_info("allocate memory of cpu%d array\n", i);
			ti->trow = (trow_t *)vmalloc(sizeof(trow_t) * MAX_TRACE_IDX);
			if (ti->trow == NULL) {
				pr_err("Can't allocate memory, Profiling Off\n");
				return -ENOMEM;
			}
		}
		if (ti->trow)
			memset(ti->trow, 0, sizeof(trow_t) * MAX_TRACE_IDX);

		per_cpu(ksend_count, i) = 0;
		atomic_set(&ti->index, 0);
		atomic_set(&ti->read_index, 0);
		ti->idx_flag = 0;
		ti->read_idx_flag = 0;
		per_cpu(gseq_no, i) = 0;
		alloc_cnt++;
	}
	return 0;
}

#ifdef CONFIG_LG_SYSPROF_HWCACHE

#define P(type, op) to64(HW_CACHE_##type##_##op)

static int create_sysprof_kernel_counter(pid_t pid);
static int create_sysprof_process_counter(pid_t pid);
void create_sysprof_cpu_counter(int cpu);
static int clean_perf_cache_tracing(void);

extern pid_t sysprof_hwcache_pid;

inline u64 to64(u32 data) __attribute__ ((always_inline));
u64 to64(u32 data)
{
	return (u64)data;
}

extern int perf_max_events;
unsigned long long predef_event[PREDEF_MAX_COUNT]; 

#if 0
unsigned long long predef_event[PREDEF_MAX_COUNT] = {
	[L1D_MISS] = P(HW_CACHE_L1D_MISS_L, HW_CACHE_L1D_MISS_H)
	[L1I_MISS] = P(HW_CACHE_L1I_MISS_L, HW_CACHE_L1I_MISS_H),
	[DTLB_MISS] = P(HW_CACHE_DTLB_MISS_L, HW_CACHE_DTLB_MISS_H),
	[ITLB_MISS] = P(HW_CACHE_ITLB_MISS_L, HW_CACHE_ITLB_MISS_H),
	[BPU_MISS] = P(HW_CACHE_BPU_MISS_L, HW_CACHE_BPU_MISS_H)
};
#endif

static int available_max_events;
static struct perf_event_attr *evt_attr;
static struct perf_event **perf_events_cpu[nr_cpumask_bits];
static struct perf_event **perf_events_proc;

static void sysprof_predef_event_init(void)
{
	predef_event[L1D_MISS] = P(L1D, MISS);
#if 0
	predef_event[L1I_MISS] = P(L1I, MISS);
	predef_event[DTLB_MISS] = P(DTLB, MISS);
	predef_event[ITLB_MISS] = P(ITLB, MISS);
	predef_event[BPU_MISS] = P(BPU, MISS);
#endif
}

/*
 * memory allocate evt_attr, perf_events_cpu[cpu] variable
 */
static int sysprof_perf_init(void)
{
	int cpu;

	available_max_events = perf_max_events;
	if (perf_max_events > PREDEF_MAX_COUNT)
		available_max_events = PREDEF_MAX_COUNT;
	pr_info("available %d, perf max %d nrcpu %d\n", available_max_events, perf_max_events, nr_cpumask_bits);

	evt_attr = kcalloc(available_max_events, sizeof(struct perf_event_attr), GFP_KERNEL);
	if (!evt_attr) {
		pr_err("Failed to allocate %d counters\n", available_max_events);
		return -ENOMEM;
	}
	atomic_inc(&evt_attr_cnt);

	for_each_possible_cpu(cpu) {
		perf_events_cpu[cpu] = kcalloc(available_max_events, sizeof(struct perf_event *), GFP_KERNEL);
		if (!perf_events_cpu[cpu]) {
			pr_err("failed to allocate %d perf events for cpu %d\n", available_max_events, cpu);
			while (--cpu >= 0) {
				kfree(perf_events_cpu[cpu]);
			}
			return -ENOMEM;
		}
	}
	pr_info("perf_events_cpu allocated\n");
	perf_events_proc = kcalloc(available_max_events, sizeof(struct perf_event *), GFP_KERNEL);
	if (!perf_events_proc)
		pr_err("failed to allocate %d\n", available_max_events);
	pr_info("perf_events_proc allocated\n");

	sysprof_predef_event_init();
	return 0;
}

/* initialize perf_event_attr using predefined event */
static void sysprof_setup_event_attr(void)
{
	u32 size = sizeof(struct perf_event_attr);
	struct perf_event_attr *attr;

#if 0
	int i;
	for (i = 0; i < available_max_events; i++) {
		attr = &evt_attr[i];
		memset(attr, 0, size);
		attr->type = PERF_TYPE_HW_CACHE;
		attr->size = size;
		attr->config = predef_event[i];
		attr->sample_period = 0;
		attr->pinned        = 1;
	}
#else	/* check only L1 data cache miss */
	attr = &evt_attr[0];
	memset(attr, 0, size);
	attr->type = PERF_TYPE_HW_CACHE;
	attr->size = size;
	attr->config = predef_event[0];
	attr->sample_period = 0;
	attr->pinned        = 1;
	attr->inherit		= 1;
	pr_info("attr->config 0x%08llx 0x%08x\n", predef_event[0], HW_CACHE_L1D_MISS);
#endif
}

//perf_overflow_handler_t sysprof_overflow;
void sysprof_overflow(struct perf_event *event, int nmi, struct perf_sample_data *data, struct pt_regs *regs)
{
	/* TODO : implementation */
	pr_info("Not Yet Implementated\n");
}

inline static int create_sysprof_kernel_counter(pid_t pid) __attribute__ ((always_inline));

static int create_sysprof_kernel_counter(pid_t pid)
{
	int cpu, ret = 0;

	/* default pid value is -1 */
	if (pid == 0) {
		for_each_online_cpu(cpu)
			create_sysprof_cpu_counter(cpu);
	} else if (pid > 0)
		ret = create_sysprof_process_counter(pid);
	return ret;

}

static int create_sysprof_process_counter(pid_t pid)
{
	int evt;
	struct perf_event *pevent;
	for (evt = 0; evt < available_max_events; evt++) {
		pevent = perf_event_create_kernel_counter(&evt_attr[evt], -1, pid, sysprof_overflow);
		if (IS_ERR(pevent))
			goto error_exit;
		perf_events_proc[evt] = pevent;
		atomic_inc(&perf_event_cnt);
		pr_info("perf event created >> perf_events_proc[%d]:%p\n", evt, perf_events_proc[evt]);
	}
	return 0;
error_exit:
	for (evt = evt - 1; evt > 0; evt--) {
		if (perf_events_proc[evt]) {
			perf_event_release_kernel(perf_events_proc[evt]);
			perf_events_proc[evt] = NULL;
		}
	}
	return PTR_ERR(pevent);
}
inline void release_sysprof_proc_counter(void)
{
	int evt;
	struct perf_event *event;

	for (evt = 0; evt < available_max_events; evt++) {
		event = perf_events_proc[evt];
		if (event != NULL) {
			pr_info("perf event released >> perf_events_proc[%d]:%p\n", evt, event);
			perf_event_release_kernel(event);
			perf_events_proc[evt] = NULL;
			atomic_dec(&perf_event_cnt);	/* have to check */
		} else {
			pr_err("event is NULL, perf_events_proc[%d]\n", evt);
		}
	}
}

void create_sysprof_cpu_counter(int cpu)
{
	int evt;

	for (evt = 0; evt < available_max_events; evt++) {
		if (perf_events_cpu[cpu][evt] != NULL) {
			pr_info("?????????????? Why not NULL???\n");
			continue;
		}
		perf_events_cpu[cpu][evt] =
			perf_event_create_kernel_counter(&evt_attr[evt], cpu, -1, sysprof_overflow);
		atomic_inc(&perf_event_cnt);
	}
}

inline void release_sysprof_cpu_counter(int cpu) __attribute__ ((always_inline));

void release_sysprof_cpu_counter(int cpu)
{
	int evt;
	struct perf_event *event;

	for (evt = 0; evt < available_max_events; evt++) {
		event = perf_events_cpu[cpu][evt];
		if (event != NULL) {
			pr_info("perf_event_release_kernel called\n");
			perf_event_release_kernel(event);
			perf_events_cpu[cpu][evt] = NULL;
			atomic_dec(&perf_event_cnt);	/* have to check */
		} else {
			pr_err("event is NULL, perf_events_cpu[%d][%d]\n", cpu, evt);
		}
	}
}

int prepare_perf_cache_tracing(void)
{
	int ret = 0;

	ret = sysprof_perf_init();
	if (ret)
		return ret;
	sysprof_setup_event_attr();

	pr_info("%s available_max_event %d, target pid %d\n", __func__, available_max_events, sysprof_hwcache_pid);
	ret = create_sysprof_kernel_counter(sysprof_hwcache_pid);
	if (ret)
		clean_perf_cache_tracing();

#if 0
	for_each_online_cpu(cpu) {
		create_sysprof_kernel_counter(cpu);
	}
#endif

	return ret;
}

static int clean_perf_cache_tracing(void)
{
	int cpu;

	printk("%s %d\n", __func__, __LINE__);
	for_each_online_cpu(cpu) {
	printk("%s : release_sysprof_cpu_counter called\n", __func__);
		release_sysprof_cpu_counter(cpu);
		kfree(perf_events_cpu[cpu]);
		perf_events_cpu[cpu] = NULL;
	}

	printk("%s : release_sysprof_proc_counter called\n", __func__);
	release_sysprof_proc_counter();
	kfree(perf_events_proc);
	perf_events_proc = NULL;

	if (evt_attr) {
		kfree(evt_attr);
		atomic_dec(&evt_attr_cnt);
	}
	available_max_events = 0;
	return 0;
}

void sysprof_perf_event_read(trace_info_t *ti, int idx, int cpu, pid_t pid)
{
	int i = 0;
	struct perf_event_context *ctx;
	struct perf_event *event;
	u64 enabled, running;

	ctx = sysprof_find_get_context(cpu);
//	ctx = find_get_context(pid, cpu);

	if (ctx == NULL) {
		pr_err("ctx is NULL\n");
		return;
	}
	if (!ti) {
		pr_err("ti is NULL\n");
		return;
	}

	list_for_each_entry(event, &ctx->event_list, event_entry) {
		if (event == NULL) {
			pr_err("sysprof: event is NULL\n");
			continue;
		}
		ti->trow[idx].u.hwcache_data[i++] =
			perf_event_read_value_direct(event, &enabled, &running);
//		pr_info("event %p, pid %d count[%d] %llu enabled %llu running %llu\n", event, current->pid, i-1, ti->trow[idx].hwcache_data[i-1], enabled, running);
	}
}

static int __cpuinit
sysprof_perf_notify(struct notifier_block *self, unsigned long action, void *hcpu)
{
	unsigned int cpu = (long)hcpu;

	switch (action) {

	case CPU_UP_PREPARE:
	case CPU_UP_PREPARE_FROZEN:
		pr_info("CPU_UP_PREPARE\n");
		break;

	case CPU_ONLINE:
	case CPU_ONLINE_FROZEN:
		if (task_trace_flag) {
			if (sysprof_hwcache_pid == 0)
				create_sysprof_cpu_counter(cpu);
		}
		pr_info("CPU_ONLINE\n");
		break;

	case CPU_DOWN_PREPARE:
	case CPU_DOWN_PREPARE_FROZEN:
		pr_info("CPU_DOWN_PREPARE\n");
		break;

	case CPU_DEAD:
	case CPU_DEAD_FROZEN:
		if (task_trace_flag) {
			if (sysprof_hwcache_pid == 0) {
				release_sysprof_cpu_counter(cpu);
				kfree(perf_events_cpu[cpu]);
				perf_events_cpu[cpu] = NULL;
			}
		}
		pr_info("CPU_DEAD\n");
		break;

	default:
		break;
	}

	return NOTIFY_OK;
}

static struct notifier_block __cpuinitdata sysprof_perf_cpu_nb = {
	.notifier_call = sysprof_perf_notify,
};

static int __init sysprof_perf_cpu_init(void)
{
	int err;
	void *cpu = (void *)(long)smp_processor_id();

	err = sysprof_perf_notify(&sysprof_perf_cpu_nb, CPU_UP_PREPARE, cpu);
	BUG_ON(err == NOTIFY_BAD);
	sysprof_perf_notify(&sysprof_perf_cpu_nb, CPU_ONLINE, cpu);
	register_cpu_notifier(&sysprof_perf_cpu_nb);
	return 0;
}

early_initcall(sysprof_perf_cpu_init);

static ssize_t
sysprof_hwcache_pid_write(struct file *file, const char __user *buf,
		size_t count, loff_t *offset)
{
	char tmp_buf[64];

	if (count > sizeof(tmp_buf))
		return -EINVAL;

	if (copy_from_user(&tmp_buf, buf, count))
		return -EFAULT;

	tmp_buf[count] = '\0';

	strict_strtol(tmp_buf, 10, (long *)&sysprof_hwcache_pid);
	return 0;
}
static int sysprof_hwcache_pid_show(struct seq_file *m, void *v)
{
	SEQ_printf(m, "%d\n", sysprof_hwcache_pid);
	return 0;
}

static int sysprof_hwcache_pid_open(struct inode *inode, struct file *filp)
{
	return single_open(filp, sysprof_hwcache_pid_show, NULL);
}

static const struct file_operations sysprof_hwcache_pid_fops = {
	.open		= sysprof_hwcache_pid_open,
	.read		= seq_read,
	.write		= sysprof_hwcache_pid_write,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init init_hwcache_pid(void)
{
	struct proc_dir_entry *pe;

	pe = proc_create("sysprof_hwcache_pid", 0644, NULL, &sysprof_hwcache_pid_fops);
	if (!pe)
		return -ENOMEM;

	return 0;
}

__initcall(init_hwcache_pid);

#endif	/* CONFIG_LG_SYSPROF_HWCACHE */

void init_task_tracing(void)
{
	int i, ret = 0;

	ret = prepare_task_tracing();
	if (ret)
		return;

#ifdef CONFIG_LG_SYSPROF_KEVENT
	ret = prepare_kevent_tracing();
	if (ret)
		return;
#endif

	task_trace_flag = 1;

	for (i = 0; i < NR_CPUS; i++) {
		pr_info("wake_up sysprofd[%d] \n", i);
		wakeup_sysprofd(i);
	}

#ifdef CONFIG_LG_SYSPROF_HWCACHE
	ret = prepare_perf_cache_tracing();
	if (ret)
		return;
#endif

	trace_has_started = 1;
}

void stop_task_tracing(void)
{
	int i;

	DECLARE_WAITQUEUE(wait, current);

	trace_has_started = 0;
	task_trace_flag = 0;
	stop_wait = 1;

	for (i = 0; i < NR_CPUS; i++) {
		pr_info("wake_up sysprofd[%d] \n", i);
		wakeup_sysprofd(i);
	}

	/* wait for syncing all profile log */
	add_wait_queue(&trace_stop_wait, &wait);
	set_current_state(TASK_INTERRUPTIBLE);
	schedule();
	set_current_state(TASK_RUNNING);
	remove_wait_queue(&trace_stop_wait, &wait);
	stop_wait = 0;
#ifdef CONFIG_LG_SYSPROF_HWCACHE
	sysprof_hwcache_pid = -1;
#endif
}

void trace_done_wakeup(int cpu)
{
	trace_info_t *ti;
	kevent_info_t *keinfo;

	ti = &per_cpu(task_trace, cpu);
	if (ti->trow) {
		vfree(ti->trow);
		ti->trow = NULL;
		pr_info("free trace_info_t for cpu[%d]\n", cpu);
		alloc_cnt--;
	}
#ifdef CONFIG_LG_SYSPROF_KEVENT
	keinfo = &per_cpu(kevent_trace, cpu);
	if (keinfo->log) {
		vfree(keinfo->log);
		keinfo->log = NULL;
		pr_info("free kevent_info_t for cpu[%d]\n", cpu);
	}
#endif

	if (alloc_cnt == 0) {
#ifdef CONFIG_LG_SYSPROF_HWCACHE
		clean_perf_cache_tracing();
		pr_info("perf event cnt %d evt_attr_cnt %d event_cnt %d\n",
				atomic_read(&perf_event_cnt), atomic_read(&evt_attr_cnt),
				atomic_read(&event_cnt));
#endif
		wake_up(&trace_stop_wait);
	}
}
EXPORT_SYMBOL(trace_done_wakeup);

#ifdef CONFIG_LG_SYSPROF_KEVENT
int prepare_kevent_tracing(void)
{
	int i;
	kevent_info_t *keinfo;

	for (i = 0; i < NR_CPUS; i++) {
		keinfo = &per_cpu(kevent_trace, i);
		if (keinfo->log == NULL) {
			pr_info("allocate keinfo[%d]\n", i);
			keinfo->log = vmalloc(sizeof(kevent_t) * MAX_KEVENT_IDX);
			if (keinfo->log == NULL) {
				pr_err("Can't allocate kevent memory, Profiling Off\n");
				return -ENOMEM;
			}
			memset(keinfo->log, 0, sizeof(kevent_t) * MAX_KEVENT_IDX);
		}
		atomic_set(&keinfo->write_idx, 0);
		atomic_set(&keinfo->read_idx, 0);
		spin_lock_init(&keinfo->lock);
	}
	return 0;
}

void sysprof_kevent_log(char *category, char *name, char *desc)
{
	__sysprof_kevent_log(category, name, desc, 0);
}
static void __sysprof_kevent_log(char *category, char *name, char *desc, unsigned int y_value)
{
	int cpu = smp_processor_id();
	int widx;
	int category_len, name_len, desc_len;
	kevent_info_t *keinfo = &per_cpu(kevent_trace, cpu);
	widx = atomic_read(&keinfo->write_idx);

	category_len = strlen(category) + 1;
	if (category_len > KEVENT_CATEGORY_LEN)
		category_len = KEVENT_CATEGORY_LEN;
	name_len = strlen(name) + 1;
	if (name_len > KEVENT_NAME_LEN)
		name_len = KEVENT_NAME_LEN;
	desc_len = strlen(desc) + 1;
	if (desc_len > KEVENT_DESC_LEN)
		desc_len = KEVENT_DESC_LEN;

	memset(&keinfo->log[widx], 0, sizeof(kevent_t));
	if (keinfo->log) {
		keinfo->log[widx].time = ktime_to_ns(ktime_get());
		memcpy(keinfo->log[widx].category, category, category_len);
		memcpy(keinfo->log[widx].name, name, name_len);
		memcpy(keinfo->log[widx].desc, desc, desc_len);
		keinfo->log[widx].category[KEVENT_CATEGORY_LEN - 1] = '\0';
		keinfo->log[widx].name[KEVENT_NAME_LEN - 1] = '\0';
		keinfo->log[widx].desc[KEVENT_DESC_LEN - 1] = '\0';
		keinfo->log[widx].y_value = y_value;

		if (widx == MAX_KEVENT_IDX)
			atomic_set(&keinfo->write_idx, 0);
		else
			atomic_inc(&keinfo->write_idx);
	}
}
EXPORT_SYMBOL(sysprof_kevent_log);
#endif

static ssize_t
task_trace_flag_write(struct file *file, const char __user *buf,
		size_t count, loff_t *offset)
{

#ifdef CONFIG_LG_SYSPROF_HWCACHE
	atomic_set(&event_cnt, 0);
#endif
	if (!task_trace_flag && !strncmp(buf, "1", count - 1)) {
		init_task_tracing();
		pr_info("task monitoring on\n");
	} else if (task_trace_flag && !strncmp(buf, "0", count - 1)) {
		stop_task_tracing();
		pr_info("task monitoring off\n");
	} else  {
		pr_info("task trace alreay start/stop\n");
	}
	return count;
}

static int task_trace_flag_show(struct seq_file *m, void *v)
{
	int i;
	trace_info_t *ti;

	SEQ_printf(m, "%d\n", task_trace_flag);
	for (i = 0; i < NR_CPUS; i++) {
		ti = &per_cpu(task_trace, i);
		SEQ_printf(m, "%ld ", per_cpu(ksend_count, i));
		SEQ_printf(m, "%d %d", atomic_read(&ti->index), atomic_read(&ti->read_index));
		SEQ_printf(m, "\n");
	}

	return 0;
}

static int task_trace_flag_open(struct inode *inode, struct file *filp)
{
	return single_open(filp, task_trace_flag_show, NULL);
}

static const struct file_operations task_trace_flag_fops = {
	.open		= task_trace_flag_open,
	.read		= seq_read,
	.write		= task_trace_flag_write,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init init_task_trace_flag(void)
{
	struct proc_dir_entry *pe;

	pe = proc_create("task_trace_flag", 0644, NULL, &task_trace_flag_fops);
	if (!pe)
		return -ENOMEM;

	return 0;
}

__initcall(init_task_trace_flag);

#ifdef USE_NETWORK
extern char sysprof_server[];

static ssize_t
sysprof_server_write(struct file *file, const char __user *buf,
		size_t count, loff_t *offset)
{
	memset(sysprof_server, 0, IPADDR_LEN);
	memcpy(sysprof_server, buf, count);
	return count;
}
static int sysprof_server_show(struct seq_file *m, void *v)
{
	SEQ_printf(m, "%s\n", sysprof_server);
	return 0;
}

static int sysprof_server_open(struct inode *inode, struct file *filp)
{
	return single_open(filp, sysprof_server_show, NULL);
}

static const struct file_operations sysprof_server_fops = {
	.open		= sysprof_server_open,
	.read		= seq_read,
	.write		= sysprof_server_write,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init init_sysprof_server(void)
{
	struct proc_dir_entry *pe;

	pe = proc_create("sysprof_server", 0644, NULL, &sysprof_server_fops);
	if (!pe)
		return -ENOMEM;

	return 0;
}

__initcall(init_sysprof_server);
#endif

void trace_sched_sysprof(int cpu, struct rq *rq, struct task_struct *prev, struct task_struct *next)
{
	int idx, read_idx, prev_idx;
	trace_info_t *ti;
	extra_info_t *idata;

	ti = &per_cpu(task_trace, cpu);
	idata = &per_cpu(int_data, cpu);

	idx = atomic_read(&ti->index);
	read_idx = atomic_read(&ti->read_index);

	if ((idx == read_idx) && (ti->idx_flag != ti->read_idx_flag)) {
		pr_info("debug cpu %d idx %d flag %d write and read idx is same. memory may be overwritten\n", cpu, idx, ti->idx_flag);
	}

	prev_idx = idx - 1;
	if (idx == 0)
		prev_idx = MAX_TRACE_IDX - 1;

	if (prev->state_flags & TASK_SLEEP) {
		if (ti->trow[prev_idx].pid != 0xFFFFFFFF)
			ti->trow[prev_idx].flags |= TASK_SLEEP;
		prev->state_flags &= ~TASK_SLEEP;
	}

	ti->trow[idx].pid = next->pid;
	ti->trow[idx].tgid = next->tgid;
	ti->trow[idx].prio = next->prio;

	/* TODO : check */
	memset(ti->trow[idx].name, 0, TASK_COMM_LEN);
	memcpy(ti->trow[idx].name, next->comm, strlen(next->comm));

	ti->trow[idx].start_time = ktime_to_ns(ktime_get());
	ti->trow[idx].flags = rq->nr_running | next->state_flags;

	ti->trow[idx].int_count = atomic_read(&idata->count);
	ti->trow[idx].int_usage = idata->sum;

#ifdef CONFIG_LG_SYSPROF_HWCACHE
	{
		/* TODO : check for being called in irq disabled state */
		if (sysprof_hwcache_pid == 0)
			sysprof_perf_event_read(ti, idx, cpu, -1);
	}
#endif

	/* initialize */
	memset(idata, 0, sizeof(extra_info_t));

	/* tmp */
	ti->trow[idx].seq_no = per_cpu(gseq_no, cpu)++;
	/**/

	next->state_flags &= ~(TASK_MIGRATION | TASK_WAKEUP);

	if (idx == (MAX_TRACE_IDX - 1)) {
		pr_info("cpu %d idx %d reached end of buffer, turn around\n", cpu, idx);
		atomic_set(&ti->index, 0);
		ti->idx_flag ^= 0x01;   /* toggle flag for wrap around of write index */
	}
	else
		atomic_inc(&ti->index);
}
#undef SEQ_printf
