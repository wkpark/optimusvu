#ifndef _SCHED_PROF_H
#define _SCHED_PROF_H

#include <linux/sched.h>

#define FALSE	0
#define TRUE	1

#define TASK_FLAGS_SHIFT_BIT    (24)
#define TASK_MIGRATION          (0x1 << TASK_FLAGS_SHIFT_BIT)
#define TASK_SLEEP              (0x2 << TASK_FLAGS_SHIFT_BIT)
#define TASK_WAKEUP             (0x4 << TASK_FLAGS_SHIFT_BIT)

#define NR_TASK_MASK			(0x0000FFFF)

#define MAX_TRACE_IDX_BIT		(15)	/* 1 << 15 = 32768 */
//#define MAX_TRACE_IDX_BIT		(14)	/* 1 << 14 = 16384 */
#define MAX_SEND_IDX_BIT		(9)

#define MAX_TRACE_IDX			(1 << MAX_TRACE_IDX_BIT)/* 32768 */
//#define SYSPROF_HIGH_WMARK		(MAX_TRACE_IDX >> 1)	/* half of MAX_TRACE_IDX */

#define MAX_SEND_IDX			(1 << MAX_SEND_IDX_BIT)	/* 512 */
#define MAX_SEND_SIZE(type)		(MAX_SEND_IDX * sizeof(type))

#define SYSPROF_UDS_PATH		"/dev/socket/lgprofd"
#define SYSPROF_BASE_PORT		(15000)
#define SYSPROF_PORT			(SYSPROF_KEVENT_PORT + 1)
#define SYSPROF_CPU_PORT(cpu)	(SYSPROF_PORT + cpu)

#define IPADDR_LEN				(16)
#define TRACE_FILENAME			"/mnt/sdcard/trace_raw"

#ifdef CONFIG_LG_SYSPROF_KEVENT
#define MAX_KEVENT_IDX_BIT		(10)	/* 1 << 10 = 1024 */
#define MAX_KEVENT_IDX			(1 << MAX_KEVENT_IDX_BIT)
#define KEVENT_HIGH_WMARK		(MAX_KEVENT_IDX >> 1)
#define KEVENT_FILENAME			"/mnt/sdcard/kevent_raw"

#define SYSPROF_KEVENT_BASE_PORT	(SYSPROF_BASE_PORT + 32)
#define SYSPROF_KEVENT_PORT(cpu) (SYSPROF_BASE_PORT + cpu)
#endif

#define USE_DSOCKET
//#define USE_FILESYSTEM
//#define USE_NETWORK

#ifdef CONFIG_LG_SYSPROF_HWCACHE

#define HW_CACHE_TYPE_SHIFT_BIT		(0)
#define HW_CACHE_OP_SHIFT_BIT		(8)
#define HW_CACHE_RESULT_SHIFT_BIT	(16)

/* architecture dependent : for ARM */
/* L1 data cache miss */
#define HW_CACHE_L1D_MISS \
	((PERF_COUNT_HW_CACHE_L1D << HW_CACHE_TYPE_SHIFT_BIT) | \
	 (PERF_COUNT_HW_CACHE_OP_READ << HW_CACHE_OP_SHIFT_BIT) | \
	 (PERF_COUNT_HW_CACHE_RESULT_MISS << HW_CACHE_RESULT_SHIFT_BIT)) 
/* L1 instruction cache miss */
#define HW_CACHE_L1I_MISS \
	((PERF_COUNT_HW_CACHE_L1I << HW_CACHE_TYPE_SHIFT_BIT) | \
	 (PERF_COUNT_HW_CACHE_OP_READ << HW_CACHE_OP_SHIFT_BIT) | \
	 (PERF_COUNT_HW_CACHE_RESULT_MISS << HW_CACHE_RESULT_SHIFT_BIT))

/* Data TLB miss */
#define HW_CACHE_DTLB_MISS \
	((PERF_COUNT_HW_CACHE_DTLB << HW_CACHE_TYPE_SHIFT_BIT) | \
	 (PERF_COUNT_HW_CACHE_OP_READ << HW_CACHE_OP_SHIFT_BIT) | \
	 (PERF_COUNT_HW_CACHE_RESULT_MISS << HW_CACHE_RESULT_SHIFT_BIT))

/* Instruction TLB miss */
#define HW_CACHE_ITLB_MISS \
	((PERF_COUNT_HW_CACHE_ITLB << HW_CACHE_TYPE_SHIFT_BIT) | \
	 (PERF_COUNT_HW_CACHE_OP_READ << HW_CACHE_OP_SHIFT_BIT) | \
	 (PERF_COUNT_HW_CACHE_RESULT_MISS << HW_CACHE_RESULT_SHIFT_BIT))


/* branch miss prediction */
#define HW_CACHE_BPU_MISS \
	((PERF_COUNT_HW_CACHE_BPU << HW_CACHE_TYPE_SHIFT_BIT) | \
	 (PERF_COUNT_HW_CACHE_OP_READ << HW_CACHE_OP_SHIFT_BIT) | \
	 (PERF_COUNT_HW_CACHE_RESULT_MISS << HW_CACHE_RESULT_SHIFT_BIT))

/**/

enum sysprof_event {
	L1D_MISS = 0,
	PREDEF_MAX_COUNT, /* only check L1 data cache */
	L1I_MISS,
	DTLB_MISS,
	ITLB_MISS,
	BPU_MISS
};

/*
typedef struct perf_evt_config perf_evt_config_t;
struct perf_evt_config {
	struct perf_event_attr attr;
	int event;
};
*/

#endif	/* CONFIG_LG_SYSPROF_HWCACHE */

typedef struct trace_row trow_t;
struct trace_row {
	unsigned int	seq_no;		/* temporary field */
	pid_t pid;
	pid_t tgid;
	int	prio;
	char	name[TASK_COMM_LEN];
	unsigned int flags;
	u64 start_time;
	unsigned int int_count;	/* interrupt count */
	unsigned int int_usage;	/* time spent on ISR */
	union {
		struct {
			unsigned int pf_count;
			unsigned int pf_usage;
		} pf_info;
#ifdef CONFIG_LG_SYSPROF_HWCACHE
		u64 hwcache_data[PREDEF_MAX_COUNT];
#endif
	} u;
};

typedef struct trace_info trace_info_t;
struct trace_info {
	atomic_t	index;
	atomic_t	read_index;
	short int	idx_flag;
	short int	read_idx_flag;
	trow_t 		*trow;
};

typedef struct extra_info extra_info_t;
struct extra_info {
	spinlock_t	lock;
	long long start_time;
	unsigned int sum;
	atomic_t count;
};

extern int check_write_condition(int cpu, int *cur_rp, int *cur_wp);
extern int task_trace_flag;
DECLARE_PER_CPU(unsigned long, ksend_count);
DECLARE_PER_CPU(unsigned int, gseq_no);
DECLARE_PER_CPU(trace_info_t, task_trace);

#ifdef CONFIG_LG_SYSPROF_KEVENT
#define KEVENT_CATEGORY_LEN	(32)
#define KEVENT_NAME_LEN		(32)
#define KEVENT_DESC_LEN		(64)

typedef struct kernel_event kevent_t;
struct kernel_event {
	long long time;
	char category[KEVENT_CATEGORY_LEN];
	char name[KEVENT_NAME_LEN];
	char desc[KEVENT_DESC_LEN];
	unsigned int	y_value;	/* event's y-value (%) */
};

typedef struct kevent_info kevent_info_t;
struct kevent_info {
	spinlock_t	lock;
	atomic_t write_idx;
	atomic_t read_idx;
	kevent_t *log;
};

DECLARE_PER_CPU(kevent_info_t, kevent_info);
#endif	/* CONFIG_LG_SYSPROF_KEVENT */
#endif	/* _SCHED_PROF_H */
